/**
 *
 */
package com.invenire.commons.validation.valiators;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang3.StringUtils;

/**
 * @author anurag
 *
 */
public class EmailValidator
{

	/**
	 *
	 * @param emailToBeValidated
	 * @return
	 */
	public static boolean isSemanticallyValid(String emailToBeValidated)
	{
		if (!StringUtils.isEmpty(emailToBeValidated))
		{
			if (org.apache.commons.validator.routines.EmailValidator
					.getInstance().isValid(emailToBeValidated))
				return true;
		}
		return false;
	}

	public static boolean isVerifiableValid(String emailToBeValidated)
	{
		if (!StringUtils.isEmpty(emailToBeValidated))
		{
			try
			{
				InternetAddress address = new InternetAddress(
						emailToBeValidated);
				address.validate();
				return true;
			} catch (AddressException e)
			{
				// return false;
			}
		}
		return false;
	}
}
