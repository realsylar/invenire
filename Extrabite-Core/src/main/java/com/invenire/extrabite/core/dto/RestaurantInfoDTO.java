/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * DTO class for managing RestaurantInfo details
 * 
 * @author Anurag Agrawal
 */
@XmlRootElement
public class RestaurantInfoDTO implements Serializable
{

	/**
	 *
	 */
	private static final long			serialVersionUID	= -1903674947250683673L;
	private int							restaurantId;
	private String						restaurantName;
	private ContactInfoDTO				contactInfoDTO;
	private RestaurantDisplayInfoDTO	restaurantDisplayInfoDTO;
	private ClientInfoDTO				clientInfoDTO;
	private boolean						isActive;
	private Timestamp					creationTimestamp;
	private Timestamp					modificationTimestamp;

	/**
	 * @return the restaurantId
	 */
	public int getRestaurantId()
	{
		return restaurantId;
	}

	/**
	 * @param restaurantId
	 *            the restaurantId to set
	 */
	public void setRestaurantId(int restaurantId)
	{
		this.restaurantId = restaurantId;
	}

	/**
	 * @return the restaurantName
	 */
	public String getRestaurantName()
	{
		return restaurantName;
	}

	/**
	 * @param restaurantName
	 *            the restaurantName to set
	 */
	public void setRestaurantName(String restaurantName)
	{
		this.restaurantName = restaurantName;
	}

	/**
	 * @return the contactInfoDTO
	 */
	public ContactInfoDTO getContactInfoDTO()
	{
		return contactInfoDTO;
	}

	/**
	 * @param contactInfoDTO
	 *            the contactInfoDTO to set
	 */
	public void setContactInfoDTO(ContactInfoDTO contactInfoDTO)
	{
		this.contactInfoDTO = contactInfoDTO;
	}

	/**
	 * @return the restaurantDisplayInfoDTO
	 */
	public RestaurantDisplayInfoDTO getRestaurantDisplayInfoDTO()
	{
		return restaurantDisplayInfoDTO;
	}

	/**
	 * @param restaurantDisplayInfoDTO
	 *            the restaurantDisplayInfoDTO to set
	 */
	public void setRestaurantDisplayInfoDTO(
			RestaurantDisplayInfoDTO restaurantDisplayInfoDTO)
	{
		this.restaurantDisplayInfoDTO = restaurantDisplayInfoDTO;
	}

	/**
	 * @return the clientInfoDTO
	 */
	public ClientInfoDTO getClientInfoDTO()
	{
		return clientInfoDTO;
	}

	/**
	 * @param clientInfoDTO
	 *            the clientInfoDTO to set
	 */
	public void setClientInfoDTO(ClientInfoDTO clientInfoDTO)
	{
		this.clientInfoDTO = clientInfoDTO;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

}
