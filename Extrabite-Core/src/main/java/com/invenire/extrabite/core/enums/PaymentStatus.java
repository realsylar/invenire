/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum PaymentStatus
{

	PAYMENT_DONE(0), PAYMENT_PENDING(1), PAYMENT_STARTED(2), PAYMENT_IN_PROGRESS(
			3);

	private final int	value;

	/**
	 * @param value
	 */
	private PaymentStatus(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static PaymentStatus getEnumForValue(int value)
	{
		for (PaymentStatus status : values())
		{
			if (status.getValue() == value)
			{
				return status;
			}
		}
		return null;
	}

	public static String getEnumNameForValue(int value)
	{
		for (PaymentStatus status : values())
		{
			if (status.getValue() == value)
			{
				return status.name();
			}
		}
		return null;
	}
}
