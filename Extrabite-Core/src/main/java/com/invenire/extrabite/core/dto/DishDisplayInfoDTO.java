/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;

/**
 * DTO class for managing the DishDisplayInfo details
 *
 * @author Anurag Agrawal
 */
public class DishDisplayInfoDTO implements Serializable
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;
	private int					dishDisplayInfoId;
	private double				price;
	private String				dishDescription;
	private int					dishSpiceLevel;
	private int					dishType;

	/**
	 * @return the price
	 */
	public double getPrice()
	{
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(double price)
	{
		this.price = price;
	}

	/**
	 * @return the dishDescription
	 */
	public String getDishDescription()
	{
		return dishDescription;
	}

	/**
	 * @param dishDescription
	 *            the dishDescription to set
	 */
	public void setDishDescription(String dishDescription)
	{
		this.dishDescription = dishDescription;
	}

	/**
	 * @return the dishSpiceLevel
	 */
	public int getDishSpiceLevel()
	{
		return dishSpiceLevel;
	}

	/**
	 * @param dishSpiceLevel
	 *            the dishSpiceLevel to set
	 */
	public void setDishSpiceLevel(int dishSpiceLevel)
	{
		this.dishSpiceLevel = dishSpiceLevel;
	}

	/**
	 * @return the dishType
	 */
	public int getDishType()
	{
		return dishType;
	}

	/**
	 * @param dishType
	 *            the dishType to set
	 */
	public void setDishType(int dishType)
	{
		this.dishType = dishType;
	}

	/**
	 * @return the dishDisplayInfoId
	 */
	public int getDishDisplayInfoId()
	{
		return dishDisplayInfoId;
	}

	/**
	 * @param dishDisplayInfoId
	 *            the dishDisplayInfoId to set
	 */
	public void setDishDisplayInfoId(int dishDisplayInfoId)
	{
		this.dishDisplayInfoId = dishDisplayInfoId;
	}

}
