/**
 *
 */
package com.invenire.extrabite.core.persistance.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * DB entity class corresponding to the restaurant_info table
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "restaurant_info")
public class RestaurantInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "restaurant_id")
	private int						restaurantId;

	@Column(name = "restaurant_name")
	private String					restaurantName;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contact_info_id")
	private ContactInfo				contactInfo;

	@OneToOne
	@JoinColumn(name = "display_info_id")
	private RestaurantDisplayInfo	restaurantDisplayInfo;

	@ManyToOne
	@JoinColumn(name = "client_id")
	private ClientInfo				clientInfo;

	@Column(name = "is_active")
	private boolean					isActive;

	@Column(name = "creation_timestamp")
	private Timestamp				creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp				modificationTimestamp;

	/**
	 * @return the restaurantId
	 */
	public int getRestaurantId()
	{
		return restaurantId;
	}

	/**
	 * @param restaurantId
	 *            the restaurantId to set
	 */
	public void setRestaurantId(int restaurantId)
	{
		this.restaurantId = restaurantId;
	}

	/**
	 * @return the restaurantName
	 */
	public String getRestaurantName()
	{
		return restaurantName;
	}

	/**
	 * @param restaurantName
	 *            the restaurantName to set
	 */
	public void setRestaurantName(String restaurantName)
	{
		this.restaurantName = restaurantName;
	}

	/**
	 * @return the contactInfo
	 */
	public ContactInfo getContactInfo()
	{
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(ContactInfo contactInfo)
	{
		this.contactInfo = contactInfo;
	}

	/**
	 * @return the restaurantDisplayInfo
	 */
	public RestaurantDisplayInfo getRestaurantDisplayInfo()
	{
		return restaurantDisplayInfo;
	}

	/**
	 * @param restaurantDisplayInfo
	 *            the restaurantDisplayInfo to set
	 */
	public void setRestaurantDisplayInfo(
			RestaurantDisplayInfo restaurantDisplayInfo)
	{
		this.restaurantDisplayInfo = restaurantDisplayInfo;
	}

	/**
	 * @return the client
	 */
	public ClientInfo getClientInfo()
	{
		return clientInfo;
	}

	/**
	 * @param client
	 *            the client to set
	 */
	public void setClientInfo(ClientInfo clientInfo)
	{
		this.clientInfo = clientInfo;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

}
