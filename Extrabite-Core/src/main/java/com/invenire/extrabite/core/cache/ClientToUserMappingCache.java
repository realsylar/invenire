/**
 *
 */
package com.invenire.extrabite.core.cache;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.invenire.extrabite.core.persistance.dao.ClientToUserMappingDAO;
import com.invenire.extrabite.core.persistance.entity.ClientToUserMapping;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ClientToUserMappingCache
{

	private static final Logger		logger				= Logger.getLogger(ClientToUserMappingCache.class);

	private Map<Integer, Integer>	userIdVsClientId	= Maps.newHashMap();
	private ClientToUserMappingDAO	mappingDAO;

	@Autowired
	public ClientToUserMappingCache(ClientToUserMappingDAO mappingDAO)
	{
		this.mappingDAO = mappingDAO;
	}

	@PostConstruct
	@Transactional(propagation = Propagation.REQUIRED)
	public void loadUserClientMappings()
	{
		List<ClientToUserMapping> list = mappingDAO
				.loadClientUserMappingFromDB();
		userIdVsClientId = Maps.newHashMap();
		for (ClientToUserMapping keyValue : list)
		{
			userIdVsClientId.put(keyValue.getUserId(), keyValue.getClientId());
		}
		logger.info("The map loaded from DB is : " + userIdVsClientId);
	}

	public Integer getProperty(int key)
	{
		return userIdVsClientId.get(key);
	}

}
