/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * A basic high level categorization of the menu into the high level categories
 * of services. This will be further categorized by either the in-house or
 * configurable categories.
 * 
 * @author Anurag Agrawal
 */
public enum MenuType
{
	VEGETARIAN(1), NONVEGETARIAN(2), LIQUOR(3);

	private final int	value;

	/**
	 * @param value
	 */
	private MenuType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

}
