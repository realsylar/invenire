/**
 *
 */
package com.invenire.extrabite.core.persistance.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.invenire.extrabite.core.persistance.entity.ClientInfo;

/**
 * DAO class for all DB operations on the client_info table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class ClientInfoDAO
{

	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public ClientInfoDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

	/**
	 * @return the sessionFactory
	 */
	private SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}

	/**
	 * A method to save the client info object to DB.
	 *
	 * @param clientInfo
	 *            THe client info object to be saved
	 * @return the identifier of the new object created in DB.
	 */
	public int saveClientInfo(ClientInfo clientInfo)
	{
		Session sess = getSessionFactory().getCurrentSession();
		int clientInfoId = (Integer) sess.save(clientInfo);
		return clientInfoId;
	}

	/**
	 * Save or update method for the Client Info objects. This will update any
	 * persisted object and will create a new object for the one's which are not
	 * already persisted
	 *
	 * @param clientInfo
	 *            The client info object to be saved or updated.
	 */
	public void saveOrUpdateClientInfo(ClientInfo clientInfo)
	{
		Session sess = getSessionFactory().getCurrentSession();
		sess.saveOrUpdate(clientInfo);
	}

	/**
	 * To read the client info object present in the DB using the unique id
	 * value.
	 *
	 * @param clientInfoId
	 *            The unique id corresponding to the client to be looked up
	 * @return The client info object pertaining to the unique id. Null if not
	 *         found in DB.
	 */
	public ClientInfo getClientInfoById(int clientInfoId)
	{
		Session sess = getSessionFactory().getCurrentSession();
		ClientInfo clientInfo = (ClientInfo) sess.get(ClientInfo.class,
				clientInfoId);
		return clientInfo;
	}

	/**
	 * To read the client info object present in the DB using the unique email
	 * id of the user.
	 *
	 * @param email
	 *            The unique email id corresponding to the client to be looked
	 *            up
	 * @return The client info object pertaining to the unique email id. Null if
	 *         not found in DB.
	 */
	public ClientInfo getClientInfoByEmail(String email)
	{
		Session sess = getSessionFactory().getCurrentSession();
		Criteria crit = sess.createCriteria(ClientInfo.class);
		crit.add(Restrictions.eq("email", email));
		ClientInfo clientInfo = (ClientInfo) crit.uniqueResult();
		return clientInfo;
	}

	/**
	 * To read the client info object present in the DB using the unique id
	 * value and the isActive flag.
	 *
	 * @param clientInfoId
	 *            The unique id corresponding to the client to be looked up
	 * @param isActive
	 *            The value of the flag which can be used to get specific
	 *            selection according to the flag value
	 * @return The client info object pertaining to the unique id and the
	 *         isActive flag. Null if not found in DB.
	 */
	public ClientInfo getActiveClientInfoById(int clientInfoId, boolean isActive)
	{
		Session sess = getSessionFactory().getCurrentSession();
		Criteria crit = sess.createCriteria(ClientInfo.class);
		crit.add(Restrictions.eq("clientId", clientInfoId)).add(
				Restrictions.eq("isActive", isActive));
		ClientInfo clientInfo = (ClientInfo) crit.uniqueResult();
		return clientInfo;
	}

	/**
	 * To read the client info object present in the DB using the unique email
	 * id of the user and the isActive flag.
	 *
	 * @param email
	 *            The unique email id corresponding to the client to be looked
	 *            up
	 * @param isActive
	 *            The value of the flag which can be used to get specific
	 *            selection according to the flag value
	 * @return The client info object pertaining to the unique email id and the
	 *         isActive flag. Null if not found in DB.
	 */
	public ClientInfo getActiveClientInfoByEmail(String email)
	{
		Session sess = getSessionFactory().getCurrentSession();
		Criteria crit = sess.createCriteria(ClientInfo.class);
		crit.add(Restrictions.eq("email", email)).add(
				Restrictions.eq("isActive", true));
		ClientInfo clientInfo = (ClientInfo) crit.uniqueResult();
		return clientInfo;
	}
}
