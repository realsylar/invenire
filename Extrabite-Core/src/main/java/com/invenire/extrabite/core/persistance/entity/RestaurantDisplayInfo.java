/**
 *
 */
package com.invenire.extrabite.core.persistance.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * DB entity class corresponding to the restaurant_display_info table
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "restaurant_display_info")
public class RestaurantDisplayInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "restaurant_display_info_id")
	private int						restaurantDisplayInfoId;

	@OneToMany
	@JoinTable(name = "restaurant_menu_mapping", joinColumns = @JoinColumn(name = "restaurant_display_info_id"), inverseJoinColumns = @JoinColumn(name = "menu_id"))
	private Set<RestaurantMenuInfo>	completeMenu;

	/**
	 * @return the restaurantDisplayInfoId
	 */
	public int getRestaurantDisplayInfoId()
	{
		return restaurantDisplayInfoId;
	}

	/**
	 * @param restaurantDisplayInfoId
	 *            the restaurantDisplayInfoId to set
	 */
	public void setRestaurantDisplayInfoId(int restaurantDisplayInfoId)
	{
		this.restaurantDisplayInfoId = restaurantDisplayInfoId;
	}

	/**
	 * @return the completeMenu
	 */
	public Set<RestaurantMenuInfo> getCompleteMenu()
	{
		return completeMenu;
	}

	/**
	 * @param completeMenu
	 *            the completeMenu to set
	 */
	public void setCompleteMenu(Set<RestaurantMenuInfo> completeMenu)
	{
		this.completeMenu = completeMenu;
	}

}
