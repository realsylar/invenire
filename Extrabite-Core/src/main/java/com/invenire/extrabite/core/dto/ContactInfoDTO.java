/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;

/**
 * General DTO class for managing contact details
 *
 * @author Anurag Agrawal
 */
public class ContactInfoDTO implements Serializable
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	private int					contactInfoId;
	private String				phoneNumber;
	private String				address;
	private int					pincode;
	private String				city;
	private String				country;

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country)
	{
		this.country = country;
	}

	/**
	 * @return the contactInfoId
	 */
	public int getContactInfoId()
	{
		return contactInfoId;
	}

	/**
	 * @param contactInfoId
	 *            the contactInfoId to set
	 */
	public void setContactInfoId(int contactInfoId)
	{
		this.contactInfoId = contactInfoId;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address)
	{
		this.address = address;
	}

	/**
	 * @return the pincode
	 */
	public int getPincode()
	{
		return pincode;
	}

	/**
	 * @param pincode
	 *            the pincode to set
	 */
	public void setPincode(int pincode)
	{
		this.pincode = pincode;
	}

}
