/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * DTO class for managing ClientInfo details
 *
 * @author Anurag Agrawal
 */
public class ClientInfoDTO implements Serializable
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	private int					clientId;
	private String				clientName;
	private String				clientDesc;
	private String				email;
	private boolean				isActive;
	private ContactInfoDTO		contactInfoDTO;
	private Timestamp			creationTimestamp;
	private Timestamp			modificationTimestamp;
	private String				password;

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the clientId
	 */
	public int getClientId()
	{
		return clientId;
	}

	/**
	 * @param clientId
	 *            the clientId to set
	 */
	public void setClientId(int clientId)
	{
		this.clientId = clientId;
	}

	/**
	 * @return the clientName
	 */
	public String getClientName()
	{
		return clientName;
	}

	/**
	 * @param clientName
	 *            the clientName to set
	 */
	public void setClientName(String clientName)
	{
		this.clientName = clientName;
	}

	/**
	 * @return the clientDesc
	 */
	public String getClientDesc()
	{
		return clientDesc;
	}

	/**
	 * @param clientDesc
	 *            the clientDesc to set
	 */
	public void setClientDesc(String clientDesc)
	{
		this.clientDesc = clientDesc;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the contactInfoDTO
	 */
	public ContactInfoDTO getContactInfoDTO()
	{
		return contactInfoDTO;
	}

	/**
	 * @param contactInfoDTO
	 *            the contactInfoDTO to set
	 */
	public void setContactInfoDTO(ContactInfoDTO contactInfoDTO)
	{
		this.contactInfoDTO = contactInfoDTO;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

}
