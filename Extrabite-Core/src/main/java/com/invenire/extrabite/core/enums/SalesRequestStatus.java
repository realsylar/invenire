/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum SalesRequestStatus
{
	REQUEST_REGISTERED(1);

	private final int	value;

	/**
	 * @param value
	 */
	private SalesRequestStatus(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}
}
