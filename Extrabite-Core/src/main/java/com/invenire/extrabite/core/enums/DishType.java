/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * This is an enum to categorize the dish into types as far as processing in the
 * system is concerned. For eg : whether a dish is configurable or non
 * configurable.
 *
 * @author Anurag Agrawal
 */
public enum DishType
{

	CONFIGURABLE(0), NONCONFIGURABLE(1);

	private int	value;

	/**
	 * @param value
	 */
	private DishType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

}
