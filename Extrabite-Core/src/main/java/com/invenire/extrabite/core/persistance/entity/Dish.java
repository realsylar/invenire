/**
 *
 */
package com.invenire.extrabite.core.persistance.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * DB entity class corresponding to the dish_info table
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "dish_info")
public class Dish
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "dish_id")
	private int				dishId;

	@Column(name = "dish_name")
	private String			dishName;

	@OneToOne
	@JoinColumn(name = "display_info_id")
	private DishDisplayInfo	dishDisplayInfo;

	@Column(name = "creation_timestamp")
	private Timestamp		creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp		modificationTimestamp;

	@Column(name = "is_active")
	private boolean			isActive;

	@Column(name = "dish_status")
	private int				dishStatus;

	/**
	 * @return the dishId
	 */
	public int getDishId()
	{
		return dishId;
	}

	/**
	 * @param dishId
	 *            the dishId to set
	 */
	public void setDishId(int dishId)
	{
		this.dishId = dishId;
	}

	/**
	 * @return the dishName
	 */
	public String getDishName()
	{
		return dishName;
	}

	/**
	 * @param dishName
	 *            the dishName to set
	 */
	public void setDishName(String dishName)
	{
		this.dishName = dishName;
	}

	/**
	 * @return the dishDisplayInfo
	 */
	public DishDisplayInfo getDishDisplayInfo()
	{
		return dishDisplayInfo;
	}

	/**
	 * @param dishDisplayInfo
	 *            the dishDisplayInfo to set
	 */
	public void setDishDisplayInfo(DishDisplayInfo dishDisplayInfo)
	{
		this.dishDisplayInfo = dishDisplayInfo;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the status
	 */
	public int getDishStatus()
	{
		return dishStatus;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setDishStatus(int dishStatus)
	{
		this.dishStatus = dishStatus;
	}

}
