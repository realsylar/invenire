/**
 *
 */
package com.invenire.extrabite.core.persistance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * DB entity class corresponding to the dish_display_info table
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "dish_display_info")
public class DishDisplayInfo
{

	@Id
	@Column(name = "dish_display_info_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int		dishDisplayInfoId;

	@Column(name = "price")
	private double	price;

	@Column(name = "dish_desc")
	private String	dishDescription;

	@Column(name = "spice_level")
	private int		dishSpiceLevel;

	@Column(name = "dish_type")
	private int		dishType;

	/**
	 * @return the dishDisplayInfoId
	 */
	public int getDishDisplayInfoId()
	{
		return dishDisplayInfoId;
	}

	/**
	 * @param dishDisplayInfoId
	 *            the dishDisplayInfoId to set
	 */
	public void setDishDisplayInfoId(int dishDisplayInfoId)
	{
		this.dishDisplayInfoId = dishDisplayInfoId;
	}

	/**
	 * @return the price
	 */
	public double getPrice()
	{
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(double price)
	{
		this.price = price;
	}

	/**
	 * @return the dishDescription
	 */
	public String getDishDescription()
	{
		return dishDescription;
	}

	/**
	 * @param dishDescription
	 *            the dishDescription to set
	 */
	public void setDishDescription(String dishDescription)
	{
		this.dishDescription = dishDescription;
	}

	/**
	 * @return the dishSpiceLevel
	 */
	public int getDishSpiceLevel()
	{
		return dishSpiceLevel;
	}

	/**
	 * @param dishSpiceLevel
	 *            the dishSpiceLevel to set
	 */
	public void setDishSpiceLevel(int dishSpiceLevel)
	{
		this.dishSpiceLevel = dishSpiceLevel;
	}

	/**
	 * @return the dishType
	 */
	public int getDishType()
	{
		return dishType;
	}

	/**
	 * @param dishType
	 *            the dishType to set
	 */
	public void setDishType(int dishType)
	{
		this.dishType = dishType;
	}

}
