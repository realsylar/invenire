/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;
import java.sql.Date;

/**
 * DTO class for managing user personal details
 *
 * @author Anurag Agrawal
 */
public class UserPersonalInfoDTO implements Serializable
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	private int					userPersonalInfoId;
	private int					userId;
	private String				currentCity;
	private String				relationshipStatus;
	private Date				dob;
	private String				education;
	private String				currentWorkplace;
	private String				likes;
	private String				sports;
	private String				music;
	private String				movies;
	private String				tvShows;
	private String				books;

	/**
	 * @return the userPersonalInfoId
	 */
	public int getUserPersonalInfoId()
	{
		return userPersonalInfoId;
	}

	/**
	 * @param userPersonalInfoId
	 *            the userPersonalInfoId to set
	 */
	public void setUserPersonalInfoId(int userPersonalInfoId)
	{
		this.userPersonalInfoId = userPersonalInfoId;
	}

	/**
	 * @return the currentCity
	 */
	public String getCurrentCity()
	{
		return currentCity;
	}

	/**
	 * @param currentCity
	 *            the currentCity to set
	 */
	public void setCurrentCity(String currentCity)
	{
		this.currentCity = currentCity;
	}

	/**
	 * @return the relationshipStatus
	 */
	public String getRelationshipStatus()
	{
		return relationshipStatus;
	}

	/**
	 * @param relationshipStatus
	 *            the relationshipStatus to set
	 */
	public void setRelationshipStatus(String relationshipStatus)
	{
		this.relationshipStatus = relationshipStatus;
	}

	/**
	 * @return the dob
	 */
	public Date getDob()
	{
		return dob;
	}

	/**
	 * @param dob
	 *            the dob to set
	 */
	public void setDob(Date dob)
	{
		this.dob = dob;
	}

	/**
	 * @return the education
	 */
	public String getEducation()
	{
		return education;
	}

	/**
	 * @param education
	 *            the education to set
	 */
	public void setEducation(String education)
	{
		this.education = education;
	}

	/**
	 * @return the currentWorkplace
	 */
	public String getCurrentWorkplace()
	{
		return currentWorkplace;
	}

	/**
	 * @param currentWorkplace
	 *            the currentWorkplace to set
	 */
	public void setCurrentWorkplace(String currentWorkplace)
	{
		this.currentWorkplace = currentWorkplace;
	}

	/**
	 * @return the likes
	 */
	public String getLikes()
	{
		return likes;
	}

	/**
	 * @param likes
	 *            the likes to set
	 */
	public void setLikes(String likes)
	{
		this.likes = likes;
	}

	/**
	 * @return the sports
	 */
	public String getSports()
	{
		return sports;
	}

	/**
	 * @param sports
	 *            the sports to set
	 */
	public void setSports(String sports)
	{
		this.sports = sports;
	}

	/**
	 * @return the music
	 */
	public String getMusic()
	{
		return music;
	}

	/**
	 * @param music
	 *            the music to set
	 */
	public void setMusic(String music)
	{
		this.music = music;
	}

	/**
	 * @return the movies
	 */
	public String getMovies()
	{
		return movies;
	}

	/**
	 * @param movies
	 *            the movies to set
	 */
	public void setMovies(String movies)
	{
		this.movies = movies;
	}

	/**
	 * @return the tvShows
	 */
	public String getTvShows()
	{
		return tvShows;
	}

	/**
	 * @param tvShows
	 *            the tvShows to set
	 */
	public void setTvShows(String tvShows)
	{
		this.tvShows = tvShows;
	}

	/**
	 * @return the books
	 */
	public String getBooks()
	{
		return books;
	}

	/**
	 * @param books
	 *            the books to set
	 */
	public void setBooks(String books)
	{
		this.books = books;
	}

	/**
	 * @return the userId
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

}
