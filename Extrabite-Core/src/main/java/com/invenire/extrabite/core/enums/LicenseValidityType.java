/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum LicenseValidityType
{
	MONTHLY(1), QUARTERLY(4), YEARLY(12), HALFYEARLY(6);

	private int	value;

	/**
	 * @param value
	 */
	private LicenseValidityType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}
}
