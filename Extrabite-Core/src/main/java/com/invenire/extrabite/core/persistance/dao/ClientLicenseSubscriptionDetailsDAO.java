/**
 *
 */
package com.invenire.extrabite.core.persistance.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.invenire.extrabite.core.persistance.entity.ClientInfo;
import com.invenire.extrabite.core.persistance.entity.ClientLicenseSubscriptionDetails;

/**
 * @author Anurag Agrawal
 *
 */
@Repository
public class ClientLicenseSubscriptionDetailsDAO
{

	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public ClientLicenseSubscriptionDetailsDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

	/**
	 * @return the sessionFactory
	 */
	private SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}

	public int saveClientLicenseSubscriptionDetails(
			ClientLicenseSubscriptionDetails clientLicenseSubscriptionInfo)
	{
		Session sess = getSessionFactory().getCurrentSession();
		int subsId = (Integer) sess.save(clientLicenseSubscriptionInfo);
		return subsId;
	}

	public void saveOrUpdateClientLicenseSubscriptionDetails(
			ClientLicenseSubscriptionDetails clientLicenseSubscriptionInfo)
	{
		Session sess = getSessionFactory().getCurrentSession();
		sess.saveOrUpdate(clientLicenseSubscriptionInfo);
	}

	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsById(
			int clientLicenseSubscriptionInfoId)
	{
		Session sess = getSessionFactory().getCurrentSession();
		ClientLicenseSubscriptionDetails clientLicenseSubscriptionInfo = (ClientLicenseSubscriptionDetails) sess
				.get(ClientLicenseSubscriptionDetails.class,
						clientLicenseSubscriptionInfoId);
		return clientLicenseSubscriptionInfo;
	}

	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByClientId(
			int clientId)
	{
		Session sess = getSessionFactory().getCurrentSession();
		Criteria crit = sess
				.createCriteria(ClientLicenseSubscriptionDetails.class);
		crit.add(Restrictions.eq("clientId", clientId));
		ClientLicenseSubscriptionDetails subsInfo = (ClientLicenseSubscriptionDetails) crit
				.uniqueResult();
		return subsInfo;
	}

	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByClientId(
			int clientId, boolean isActive)
	{
		Session sess = getSessionFactory().getCurrentSession();
		Criteria crit = sess
				.createCriteria(ClientLicenseSubscriptionDetails.class);
		crit.add(Restrictions.eq("clientId", clientId)).add(
				Restrictions.eq("isActive", isActive));
		ClientLicenseSubscriptionDetails subsInfo = (ClientLicenseSubscriptionDetails) crit
				.uniqueResult();
		return subsInfo;
	}

	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByClientIdAndLicenseId(
			int clientId, int licenseId)
	{
		Session sess = getSessionFactory().getCurrentSession();
		Criteria crit = sess.createCriteria(ClientInfo.class);
		crit.add(Restrictions.eq("clientId", clientId)).add(
				Restrictions.eq("licenseId", licenseId));
		ClientLicenseSubscriptionDetails clientLicenseSubscriptionDetails = (ClientLicenseSubscriptionDetails) crit
				.uniqueResult();
		return clientLicenseSubscriptionDetails;
	}

	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByClientIdAndLicenseId(
			int clientId, int licenseId, boolean isActive)
	{
		Session sess = getSessionFactory().getCurrentSession();
		Criteria crit = sess.createCriteria(ClientInfo.class);
		crit.add(Restrictions.eq("clientId", clientId))
		.add(Restrictions.eq("licenseId", licenseId))
		.add(Restrictions.eq("isActive", isActive));
		ClientLicenseSubscriptionDetails clientLicenseSubscriptionDetails = (ClientLicenseSubscriptionDetails) crit
				.uniqueResult();
		return clientLicenseSubscriptionDetails;
	}

	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByLicenseId(
			int licenseId)
	{
		Session sess = getSessionFactory().getCurrentSession();
		Criteria crit = sess
				.createCriteria(ClientLicenseSubscriptionDetails.class);
		crit.add(Restrictions.eq("licenseId", licenseId));
		ClientLicenseSubscriptionDetails clientLicenseSubscriptionDetails = (ClientLicenseSubscriptionDetails) crit
				.uniqueResult();
		return clientLicenseSubscriptionDetails;
	}

	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByLicenseId(
			int licenseId, boolean isActive)
	{
		Session sess = getSessionFactory().getCurrentSession();
		Criteria crit = sess
				.createCriteria(ClientLicenseSubscriptionDetails.class);
		crit.add(Restrictions.eq("licenseId", licenseId)).add(
				Restrictions.eq("isActive", isActive));
		ClientLicenseSubscriptionDetails clientLicenseSubscriptionDetails = (ClientLicenseSubscriptionDetails) crit
				.uniqueResult();
		return clientLicenseSubscriptionDetails;
	}
}
