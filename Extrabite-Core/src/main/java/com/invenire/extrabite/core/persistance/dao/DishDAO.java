/**
 *
 */
package com.invenire.extrabite.core.persistance.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO class for all DB operations on the dish_info table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class DishDAO
{

	@SuppressWarnings("unused")
	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public DishDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

}
