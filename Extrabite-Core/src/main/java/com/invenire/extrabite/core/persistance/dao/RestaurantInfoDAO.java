/**
 *
 */
package com.invenire.extrabite.core.persistance.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.invenire.extrabite.core.persistance.entity.RestaurantInfo;

/**
 * DAO class for all DB operations on the restaurant_info table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class RestaurantInfoDAO
{

	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public RestaurantInfoDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

	/**
	 * A method to get the complete restaurant details by the id of the
	 * restaurant.
	 *
	 * @param id
	 *            The unique identifier for the restaurant.
	 * @return The complete restaurant info object
	 */
	public RestaurantInfo getRestaurantById(int restaurantInfoId)
	{
		return (RestaurantInfo) sessionFactory.getCurrentSession().get(
				RestaurantInfo.class, restaurantInfoId);
	}

	/**
	 * A method to get the complete restaurant details of all the restaurants
	 * owned by a client by the id of the client.
	 *
	 * @param clientId
	 *            The unique identifier for the client
	 * @return The list of complete restaurant info objects
	 */
	public List<RestaurantInfo> getRestaurantsByClientId(int clientId)
	{
		Session sess = sessionFactory.getCurrentSession();
		Criteria crit = sess.createCriteria(RestaurantInfo.class);
		crit.add(Restrictions.eq("clientId", clientId));
		List<RestaurantInfo> restaurants = crit.list();
		return restaurants;
	}

	/**
	 * A method to save the restaurant details in DB
	 *
	 * @param restaurantInfo
	 *            The restaurant info details object to be persisted
	 * @return The unique identifier id of the restaurant being persisted
	 */
	public int saveRestaurant(RestaurantInfo restaurantInfo)
	{
		Session sess = sessionFactory.getCurrentSession();
		int restaurantId = (Integer) sess.save(restaurantInfo);
		return restaurantId;
	}

	/**
	 * A method to save or update the restaurant details in DB
	 *
	 * @param restaurantInfo
	 *            The restaurant info details object to be persisted or update
	 */
	public void saveOrUpdateRestaurant(RestaurantInfo restaurantInfo)
	{
		Session sess = sessionFactory.getCurrentSession();
		sess.saveOrUpdate(restaurantInfo);
	}
}
