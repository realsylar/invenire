/**
 *
 */
package com.invenire.extrabite.core.entity.modulation;

/**
 * @author Anurag Agrawal
 *
 */
public interface CustomizableValue
{

	public CustomizableValue customizeValue(CustomizableValue value);
}
