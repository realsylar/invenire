/**
 *
 */
package com.invenire.extrabite.core.persistance.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO class for all DB operations on the menu_dish_category_info table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class MenuDishCategoryInfoDAO
{

	@SuppressWarnings("unused")
	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public MenuDishCategoryInfoDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}
}
