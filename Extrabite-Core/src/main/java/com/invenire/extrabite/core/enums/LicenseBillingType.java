/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum LicenseBillingType
{
	MONTHLY(1.0), QUARTERLY(0.8), YEARLY(0.6), HALFYEARLY(0.7);

	private double	value;

	/**
	 * @param value
	 */
	private LicenseBillingType(double value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public double getValue()
	{
		return value;
	}
}
