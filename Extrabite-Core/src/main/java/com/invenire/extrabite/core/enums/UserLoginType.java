/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * This is a basic means in which the user has logged into our system. This is
 * some data which is just stored and can be helpful in all kinds of analysis.
 * Apart from that this will also help figure out automated system at a later
 * stage as we will know on the basis of this flag as to what specific
 * information of the user should be looked at.
 *
 * @author Anurag Agrawal
 */
public enum UserLoginType
{

	INTERNAL(0), FACEBOOK(1), GOOGLE(2);

	private int	value;

	/**
	 * @param value
	 */
	private UserLoginType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

}
