/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum OfferTypes
{

	DISCOUNT(0), ONEONONE(1);

	private int	value;

	/**
	 * @param value
	 */
	private OfferTypes(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static OfferTypes getEnumForValue(int value)
	{
		for (OfferTypes type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

	public static String getEnumNameForValue(int value)
	{
		for (OfferTypes type : values())
		{
			if (type.getValue() == value)
			{
				return type.name();
			}
		}
		return null;
	}
}
