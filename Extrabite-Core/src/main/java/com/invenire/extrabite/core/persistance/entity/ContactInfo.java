/**
 *
 */
package com.invenire.extrabite.core.persistance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * General DB Entity class for managing contact details
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "contact_info")
public class ContactInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "contact_info_id")
	private int		contactInfoId;

	@Column(name = "phone_number")
	private String	phoneNumber;

	@Column(name = "address")
	private String	address;

	@Column(name = "pincode")
	private int		pincode;

	@Column(name = "city")
	private String	city;

	@Column(name = "country")
	private String	country;

	/**
	 * @return the contactInfoId
	 */
	public int getContactInfoId()
	{
		return contactInfoId;
	}

	/**
	 * @param contactInfoId
	 *            the contactInfoId to set
	 */
	public void setContactInfoId(int contactInfoId)
	{
		this.contactInfoId = contactInfoId;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address)
	{
		this.address = address;
	}

	/**
	 * @return the pincode
	 */
	public int getPincode()
	{
		return pincode;
	}

	/**
	 * @param pincode
	 *            the pincode to set
	 */
	public void setPincode(int pincode)
	{
		this.pincode = pincode;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country)
	{
		this.country = country;
	}

}
