/**
 *
 */
package com.invenire.extrabite.core.persistance.entity;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * DB entity class corresponding to the restaurant_menu_info table
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "restaurant_menu_info")
public class RestaurantMenuInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "menu_id")
	private int							menuId;

	@OneToMany
	@JoinTable(name = "restaurant_menu_category_mapping", joinColumns = @JoinColumn(name = "menu_id"), inverseJoinColumns = @JoinColumn(name = "category_id"))
	private Set<MenuDishCategoryInfo>	categoryWiseDishes;

	@Column(name = "is_active")
	private boolean						isActive;

	@Column(name = "menu_type")
	private int							menuType;

	@Column(name = "creation_timestamp")
	private Timestamp					creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp					modificationTimestamp;

	/**
	 * @return the menuId
	 */
	public int getMenuId()
	{
		return menuId;
	}

	/**
	 * @param menuId
	 *            the menuId to set
	 */
	public void setMenuId(int menuId)
	{
		this.menuId = menuId;
	}

	/**
	 * @return the categoryWiseDishes
	 */
	public Set<MenuDishCategoryInfo> getCategoryWiseDishes()
	{
		return categoryWiseDishes;
	}

	/**
	 * @param categoryWiseDishes
	 *            the categoryWiseDishes to set
	 */
	public void setCategoryWiseDishes(
			Set<MenuDishCategoryInfo> categoryWiseDishes)
	{
		this.categoryWiseDishes = categoryWiseDishes;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the menuType
	 */
	public int getMenuType()
	{
		return menuType;
	}

	/**
	 * @param menuType
	 *            the menuType to set
	 */
	public void setMenuType(int menuType)
	{
		this.menuType = menuType;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

}
