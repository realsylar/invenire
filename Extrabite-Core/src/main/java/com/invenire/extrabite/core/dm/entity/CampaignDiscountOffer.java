/**
 *
 */
package com.invenire.extrabite.core.dm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "discount_offer_info")
@PrimaryKeyJoinColumn(name = "offer_id")
public class CampaignDiscountOffer extends CampaignOffer
{

	@Column(name = "disc_percentage")
	private int	discountOfferPercentage;

	/**
	 * @return the discountOfferPercentage
	 */
	public int getDiscountOfferPercentage()
	{
		return discountOfferPercentage;
	}

	/**
	 * @param discountOfferPercentage
	 *            the discountOfferPercentage to set
	 */
	public void setDiscountOfferPercentage(int discountOfferPercentage)
	{
		this.discountOfferPercentage = discountOfferPercentage;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.invenire.extrabite.core.dm.CampaignOffers#displayPricingDetails()
	 */
	@Override
	public String displayPricingDetails()
	{
		return null;
	}

}
