/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * This scales the configurable spice level property of the dishes. Is a
 * variable property of the dishes. Can be changed by the user.
 *
 * @author Anurag Agrawal
 */
public enum DishSpiceLevel
{

	NONE(0), LOW(1), LOW_MEDIUM(3), HIGH_MEDIUM(4), HIGH(5);

	private int	value;

	/**
	 * @param value
	 */
	private DishSpiceLevel(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

}
