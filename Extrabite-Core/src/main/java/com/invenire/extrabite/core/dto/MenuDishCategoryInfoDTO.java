/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

/**
 * DTO class for managing the MenuDishCategoryInfo details
 * 
 * @author Anurag Agrawal
 */
public class MenuDishCategoryInfoDTO implements Serializable
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	private int					categoryId;
	private String				categoryName;
	private String				categoryDescription;
	private Set<DishDTO>		dishesFallingInTheCategory;
	private int					categoryType;
	private boolean				isActive;
	private Timestamp			creationTimestamp;
	private Timestamp			modificationTimestamp;

	/**
	 * @return the categoryId
	 */
	public int getCategoryId()
	{
		return categoryId;
	}

	/**
	 * @param categoryId
	 *            the categoryId to set
	 */
	public void setCategoryId(int categoryId)
	{
		this.categoryId = categoryId;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName()
	{
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}

	/**
	 * @return the categoryDescription
	 */
	public String getCategoryDescription()
	{
		return categoryDescription;
	}

	/**
	 * @param categoryDescription
	 *            the categoryDescription to set
	 */
	public void setCategoryDescription(String categoryDescription)
	{
		this.categoryDescription = categoryDescription;
	}

	/**
	 * @return the dishesFallingInTheCategory
	 */
	public Set<DishDTO> getDishesFallingInTheCategory()
	{
		return dishesFallingInTheCategory;
	}

	/**
	 * @param dishesFallingInTheCategory
	 *            the dishesFallingInTheCategory to set
	 */
	public void setDishesFallingInTheCategory(
			Set<DishDTO> dishesFallingInTheCategory)
	{
		this.dishesFallingInTheCategory = dishesFallingInTheCategory;
	}

	/**
	 * @return the categoryType
	 */
	public int getCategoryType()
	{
		return categoryType;
	}

	/**
	 * @param categoryType
	 *            the categoryType to set
	 */
	public void setCategoryType(int categoryType)
	{
		this.categoryType = categoryType;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

}
