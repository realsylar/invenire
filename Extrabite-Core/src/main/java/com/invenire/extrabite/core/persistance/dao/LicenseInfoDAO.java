/**
 *
 */
package com.invenire.extrabite.core.persistance.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * DAO class for all DB operations on the license_info table.
 *
 * @author Anurag Agrawal
 */
public class LicenseInfoDAO
{

	@SuppressWarnings("unused")
	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public LicenseInfoDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

}
