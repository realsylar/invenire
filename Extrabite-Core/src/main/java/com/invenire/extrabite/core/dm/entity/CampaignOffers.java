/**
 *
 */
package com.invenire.extrabite.core.dm.entity;

/**
 * @author Anurag Agrawal
 *
 */
public interface CampaignOffers
{

	public String displayPricingDetails();
}
