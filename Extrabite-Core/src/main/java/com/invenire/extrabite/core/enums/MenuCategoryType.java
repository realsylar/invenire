/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * This is to classify if the category of the dish is in one of the in-house
 * provided categories or some configurable category. Some default categories of
 * the menu will be provided out of box for ease of use.
 *
 * @author Anurag Agrawal
 */
public enum MenuCategoryType
{
	INTERNAL(1), CLIENT(2);

	private int	value;

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	/**
	 * @param value
	 */
	private MenuCategoryType(int value)
	{
		this.value = value;
	}

}
