/**
 *
 */
package com.invenire.extrabite.core.sec.enc;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.core.sec.enc.exception.EncryptorException;
import com.invenire.extrabite.core.utils.ExtrabiteProperties;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class AESCBCEncryptor
{

	private ExtrabiteProperties	props;
	private String				mainKey;
	private static final String	AES_ENCRYPT_MAIN_KEY	= "aes_encrypt_main_key";
	private static final Logger	logger					= Logger.getLogger(AESCBCEncryptor.class);

	/**
	 * @param props
	 * @param mainKey
	 */
	@Autowired
	public AESCBCEncryptor(ExtrabiteProperties props)
	{
		super();
		this.props = props;
		this.mainKey = null;
	}

	/**
	 *
	 */
	private void init()
	{
		if (mainKey == null || "".equals(mainKey.trim()))
			mainKey = props.getProperty(AES_ENCRYPT_MAIN_KEY);
	}

	public String encrypt(String plainMessage) throws EncryptorException
	{
		try
		{
			init();
			SecretKeySpec key = new SecretKeySpec(mainKey.getBytes("UTF-8"),
					"AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(
					new byte[16]));
			byte[] cipherText = new byte[cipher.getOutputSize(plainMessage
					.length())];
			cipherText = cipher.doFinal(plainMessage.getBytes());
			String cipherTextStr = new String(Base64.encodeBase64(cipherText),
					"UTF-8");
			return cipherTextStr;
		} catch (UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException | NoSuchAlgorithmException
				| NoSuchPaddingException e)
		{
			logger.error("Exception occured while encrypting value : "
					+ e.getMessage());
			throw new EncryptorException(e);
		}

	}

	public String decrypt(String encMessage) throws EncryptorException
	{
		try
		{
			init();
			SecretKeySpec key = new SecretKeySpec(mainKey.getBytes("UTF-8"),
					"AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(
					new byte[16]));
			byte[] encMessageWithoutBase64 = encMessage.getBytes("UTF-8");
			encMessageWithoutBase64 = Base64
					.decodeBase64(encMessageWithoutBase64);
			byte[] plainText = cipher.update(encMessageWithoutBase64);
			plainText = cipher.doFinal();
			return new String(plainText, "UTF-8");
		} catch (UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException | NoSuchAlgorithmException
				| NoSuchPaddingException e)
		{
			logger.error("Exception occured while decrypting value : "
					+ e.getMessage());
			throw new EncryptorException(e);
		}
	}
}
