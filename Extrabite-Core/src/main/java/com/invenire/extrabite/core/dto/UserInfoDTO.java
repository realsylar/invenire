/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * DTO class for managing user details
 *
 * @author Anurag Agrawal
 */
public class UserInfoDTO implements Serializable
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	private int					userId;
	private ContactInfoDTO		contactInfoDTO;
	private String				email;
	private Timestamp			creationTimestamp;
	private Timestamp			modificationTimestamp;
	private boolean				isActive;
	private int					userType;
	private UserLoginInfoDTO	loginInfoDTO;

	/**
	 * @return the userId
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * @return the contactInfo
	 */
	public ContactInfoDTO getContactInfoDTO()
	{
		return contactInfoDTO;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfoDTO(ContactInfoDTO contactInfoDTO)
	{
		this.contactInfoDTO = contactInfoDTO;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the userType
	 */
	public int getUserType()
	{
		return userType;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(int userType)
	{
		this.userType = userType;
	}

	/**
	 * @return the loginInfoDTO
	 */
	public UserLoginInfoDTO getLoginInfoDTO()
	{
		return loginInfoDTO;
	}

	/**
	 * @param loginInfoDTO
	 *            the loginInfoDTO to set
	 */
	public void setLoginInfoDTO(UserLoginInfoDTO loginInfoDTO)
	{
		this.loginInfoDTO = loginInfoDTO;
	}

}
