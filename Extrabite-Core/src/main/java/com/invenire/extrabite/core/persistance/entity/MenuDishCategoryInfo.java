/**
 *
 */
package com.invenire.extrabite.core.persistance.entity;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * DB entity class corresponding to the menu_dish_category_info table
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "menu_dish_category_info")
public class MenuDishCategoryInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "category_id")
	private int			categoryId;

	@Column(name = "category_name")
	private String		categoryName;

	@Column(name = "category_desc")
	private String		categoryDescription;

	@OneToMany
	@JoinTable(name = "menu_dish_category_mapping", joinColumns = @JoinColumn(name = "category_id"), inverseJoinColumns = @JoinColumn(name = "dish_id"))
	private Set<Dish>	dishesFallingInTheCategory;

	@Column(name = "category_type")
	private int			categoryType;

	@Column(name = "is_active")
	private boolean		isActive;

	@Column(name = "creation_timestamp")
	private Timestamp	creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp	modificationTimestamp;

	/**
	 * @return the categoryId
	 */
	public int getCategoryId()
	{
		return categoryId;
	}

	/**
	 * @param categoryId
	 *            the categoryId to set
	 */
	public void setCategoryId(int categoryId)
	{
		this.categoryId = categoryId;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName()
	{
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}

	/**
	 * @return the categoryDescription
	 */
	public String getCategoryDescription()
	{
		return categoryDescription;
	}

	/**
	 * @param categoryDescription
	 *            the categoryDescription to set
	 */
	public void setCategoryDescription(String categoryDescription)
	{
		this.categoryDescription = categoryDescription;
	}

	/**
	 * @return the dishesFallingInTheCategory
	 */
	public Set<Dish> getDishesFallingInTheCategory()
	{
		return dishesFallingInTheCategory;
	}

	/**
	 * @param dishesFallingInTheCategory
	 *            the dishesFallingInTheCategory to set
	 */
	public void setDishesFallingInTheCategory(
			Set<Dish> dishesFallingInTheCategory)
	{
		this.dishesFallingInTheCategory = dishesFallingInTheCategory;
	}

	/**
	 * @return the categoryType
	 */
	public int getCategoryType()
	{
		return categoryType;
	}

	/**
	 * @param categoryType
	 *            the categoryType to set
	 */
	public void setCategoryType(int categoryType)
	{
		this.categoryType = categoryType;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

}
