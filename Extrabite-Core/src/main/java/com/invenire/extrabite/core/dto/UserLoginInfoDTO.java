/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * DTO class for managing user logging details
 *
 * @author Anurag Agrawal
 */
public class UserLoginInfoDTO implements Serializable
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	private int					userLoginId;
	private int					userLoginType;
	private String				userName;
	private String				password;
	private String				accessToken;
	private Timestamp			modificationTimestamp;
	private Timestamp			creationTimestamp;

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the accessToken
	 */
	public String getAccessToken()
	{
		return accessToken;
	}

	/**
	 * @param accessToken
	 *            the accessToken to set
	 */
	public void setAccessToken(String accessToken)
	{
		this.accessToken = accessToken;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the userLoginId
	 */
	public int getUserLoginId()
	{
		return userLoginId;
	}

	/**
	 * @param userLoginId
	 *            the userLoginId to set
	 */
	public void setUserLoginId(int userLoginId)
	{
		this.userLoginId = userLoginId;
	}

	/**
	 * @return the userLoginType
	 */
	public int getUserLoginType()
	{
		return userLoginType;
	}

	/**
	 * @param userLoginType
	 *            the userLoginType to set
	 */
	public void setUserLoginType(int userLoginType)
	{
		this.userLoginType = userLoginType;
	}

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

}
