/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;
import java.util.Set;

/**
 * DTO class for managing the RestaurantDisplayInfo details
 * 
 * @author Anurag Agrawal
 */
public class RestaurantDisplayInfoDTO implements Serializable
{

	/**
	 *
	 */
	private static final long			serialVersionUID	= 2129620234591244943L;

	private int							restaurantDisplayInfoId;
	private Set<RestaurantMenuInfoDTO>	completeMenu;

	/**
	 * @return the restaurantDisplayInfoId
	 */
	public int getRestaurantDisplayInfoId()
	{
		return restaurantDisplayInfoId;
	}

	/**
	 * @param restaurantDisplayInfoId
	 *            the restaurantDisplayInfoId to set
	 */
	public void setRestaurantDisplayInfoId(int restaurantDisplayInfoId)
	{
		this.restaurantDisplayInfoId = restaurantDisplayInfoId;
	}

	/**
	 * @return the completeMenu
	 */
	public Set<RestaurantMenuInfoDTO> getCompleteMenu()
	{
		return completeMenu;
	}

	/**
	 * @param completeMenu
	 *            the completeMenu to set
	 */
	public void setCompleteMenu(Set<RestaurantMenuInfoDTO> completeMenu)
	{
		this.completeMenu = completeMenu;
	}

}
