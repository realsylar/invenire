/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * DTO for managing Dish details
 *
 * @author Anurag Agrawal
 */
public class DishDTO implements Serializable
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;

	private int					dishId;
	private String				dishName;
	private DishDisplayInfoDTO	dishDisplayInfoDTO;
	private Timestamp			creationTimestamp;
	private Timestamp			modificationTimestamp;
	private boolean				isActive;
	private int					dishStatus;

	/**
	 * @return the dishId
	 */
	public int getDishId()
	{
		return dishId;
	}

	/**
	 * @param dishId
	 *            the dishId to set
	 */
	public void setDishId(int dishId)
	{
		this.dishId = dishId;
	}

	/**
	 * @return the dishName
	 */
	public String getDishName()
	{
		return dishName;
	}

	/**
	 * @param dishName
	 *            the dishName to set
	 */
	public void setDishName(String dishName)
	{
		this.dishName = dishName;
	}

	/**
	 * @return the dishDisplayInfo
	 */
	public DishDisplayInfoDTO getDishDisplayInfoDTO()
	{
		return dishDisplayInfoDTO;
	}

	/**
	 * @param dishDisplayInfo
	 *            the dishDisplayInfo to set
	 */
	public void setDishDisplayInfoDTO(DishDisplayInfoDTO dishDisplayInfoDTO)
	{
		this.dishDisplayInfoDTO = dishDisplayInfoDTO;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the status
	 */
	public int getDishStatus()
	{
		return dishStatus;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setDishStatus(int dishStatus)
	{
		this.dishStatus = dishStatus;
	}

}
