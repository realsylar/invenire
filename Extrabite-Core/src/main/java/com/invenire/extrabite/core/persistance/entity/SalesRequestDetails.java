/**
 *
 */
package com.invenire.extrabite.core.persistance.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "sales_request_data")
public class SalesRequestDetails
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "request_id")
	private int			requestId;

	@Column(name = "name")
	private String		name;

	@Column(name = "phone_number")
	private String		phoneNumber;

	@Column(name = "email")
	private String		email;

	@Column(name = "comments")
	private String		comments;

	@Column(name = "request_status")
	private int			requestStatus;

	@Column(name = "city")
	private String		city;

	@Column(name = "creation_timestamp")
	private Timestamp	creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp	modificationTimestamp;

	/**
	 * @param name
	 * @param phoneNumber
	 * @param email
	 * @param city
	 */
	public SalesRequestDetails(String name, String phoneNumber, String email,
			String city)
	{
		super();
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.city = city;
	}

	/**
	 * @return the requestId
	 */
	public int getRequestId()
	{
		return requestId;
	}

	/**
	 * @param requestId
	 *            the requestId to set
	 */
	public void setRequestId(int requestId)
	{
		this.requestId = requestId;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the comments
	 */
	public String getComments()
	{
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments)
	{
		this.comments = comments;
	}

	/**
	 * @return the requestStatus
	 */
	public int getRequestStatus()
	{
		return requestStatus;
	}

	/**
	 * @param requestStatus
	 *            the requestStatus to set
	 */
	public void setRequestStatus(int requestStatus)
	{
		this.requestStatus = requestStatus;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "SalesRequestDetails [requestId=" + requestId + ", name=" + name
				+ ", phoneNumber=" + phoneNumber + ", email=" + email
				+ ", comments=" + comments + ", requestStatus=" + requestStatus
				+ ", city=" + city + ", creationTimestamp=" + creationTimestamp
				+ ", modificationTimestamp=" + modificationTimestamp + "]";
	}

}
