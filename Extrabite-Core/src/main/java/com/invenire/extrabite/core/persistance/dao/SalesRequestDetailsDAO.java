/**
 *
 */
package com.invenire.extrabite.core.persistance.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.invenire.extrabite.core.persistance.entity.SalesRequestDetails;

/**
 * DAO class for all DB operations on the sales_request_data table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class SalesRequestDetailsDAO
{

	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public SalesRequestDetailsDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

	/**
	 * @return the sessionFactory
	 */
	private SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}

	/**
	 * A method to save the sales request object to DB.
	 *
	 * @param requestDetails
	 *            The sales request object to be saved
	 * @return the identifier of the new object created in DB.
	 */
	public int saveSalesRequestDetails(SalesRequestDetails requestDetails)
	{
		Session sess = getSessionFactory().getCurrentSession();
		int requestId = (Integer) sess.save(requestDetails);
		return requestId;
	}

	/**
	 * Save or update method for the sales request objects. This will update any
	 * persisted object and will create a new object for the one's which are not
	 * already persisted
	 *
	 * @param requestDetails
	 *            The sales request object to be saved or updated.
	 */
	public void saveOrUpdateSalesRequestDetail(
			SalesRequestDetails requestDetails)
	{
		Session sess = getSessionFactory().getCurrentSession();
		sess.saveOrUpdate(requestDetails);
	}

	/**
	 * To read the sales request object present in the DB using the unique id
	 * value.
	 *
	 * @param requestId
	 *            The unique id corresponding to the request to be looked up
	 * @return The sales request object pertaining to the unique id. Null if not
	 *         found in DB.
	 */
	public SalesRequestDetails getSalesRequestDetailById(int requestId)
	{
		Session sess = getSessionFactory().getCurrentSession();
		SalesRequestDetails requestDetails = (SalesRequestDetails) sess.get(
				SalesRequestDetails.class, requestId);
		return requestDetails;
	}

	/**
	 * To read the sales request object present in the DB using the unique email
	 * id of the user.
	 *
	 * @param email
	 *            The unique email id corresponding to the sales request to be
	 *            looked up
	 * @return The sales request object pertaining to the unique email id. Null
	 *         if not found in DB.
	 */
	public SalesRequestDetails getSalesRequestDetailByEmail(String email)
	{
		Session sess = getSessionFactory().getCurrentSession();
		Criteria crit = sess.createCriteria(SalesRequestDetails.class);
		crit.add(Restrictions.eq("email", email));
		SalesRequestDetails requestDetails = (SalesRequestDetails) crit
				.uniqueResult();
		return requestDetails;
	}

}
