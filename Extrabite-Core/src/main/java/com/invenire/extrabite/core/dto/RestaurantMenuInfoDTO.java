/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

/**
 * DTO class for managing the RestaurantMenuInfo details
 *
 * @author Anurag Agrawal
 */
public class RestaurantMenuInfoDTO implements Serializable
{

	/**
	 *
	 */
	private static final long				serialVersionUID	= 1L;

	private int								menuId;
	private Set<MenuDishCategoryInfoDTO>	categoryWiseDishes;
	private boolean							isActive;
	private int								menuType;
	private Timestamp						creationTimestamp;
	private Timestamp						modificationTimestamp;

	/**
	 * @return the menuId
	 */
	public int getMenuId()
	{
		return menuId;
	}

	/**
	 * @param menuId
	 *            the menuId to set
	 */
	public void setMenuId(int menuId)
	{
		this.menuId = menuId;
	}

	/**
	 * @return the categoryWiseDishes
	 */
	public Set<MenuDishCategoryInfoDTO> getCategoryWiseDishes()
	{
		return categoryWiseDishes;
	}

	/**
	 * @param categoryWiseDishes
	 *            the categoryWiseDishes to set
	 */
	public void setCategoryWiseDishes(
			Set<MenuDishCategoryInfoDTO> categoryWiseDishes)
	{
		this.categoryWiseDishes = categoryWiseDishes;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the menuType
	 */
	public int getMenuType()
	{
		return menuType;
	}

	/**
	 * @param menuType
	 *            the menuType to set
	 */
	public void setMenuType(int menuType)
	{
		this.menuType = menuType;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

}
