/**
 *
 */
package com.invenire.extrabite.core.dm.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "offer_info")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class CampaignOffer implements CampaignOffers
{

	@Id
	@Column(name = "offer_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int			offerId;

	@Column(name = "offer_name")
	private String		offerName;

	@Column(name = "offer_desc")
	private String		offerDescription;

	@Column(name = "highlights")
	private String		highlights;

	@Column(name = "offer_num_people")
	private int			offerForNumberOfPeople;

	@Column(name = "is_valid")
	private boolean		isValid;

	@Column(name = "creation_timestamp")
	private Timestamp	creationTimestamp;

	@Column(name = "offer_start_timestamp")
	private Timestamp	offerStartTimestamp;

	@Column(name = "offer_finish_timestamp")
	private Timestamp	offerFinishTimestamp;

	/**
	 * @return the offerId
	 */
	public int getOfferId()
	{
		return offerId;
	}

	/**
	 * @param offerId
	 *            the offerId to set
	 */
	public void setOfferId(int offerId)
	{
		this.offerId = offerId;
	}

	/**
	 * @return the offerName
	 */
	public String getOfferName()
	{
		return offerName;
	}

	/**
	 * @param offerName
	 *            the offerName to set
	 */
	public void setOfferName(String offerName)
	{
		this.offerName = offerName;
	}

	/**
	 * @return the offerDescription
	 */
	public String getOfferDescription()
	{
		return offerDescription;
	}

	/**
	 * @param offerDescription
	 *            the offerDescription to set
	 */
	public void setOfferDescription(String offerDescription)
	{
		this.offerDescription = offerDescription;
	}

	/**
	 * @return the highlights
	 */
	public String getHighlights()
	{
		return highlights;
	}

	/**
	 * @param highlights
	 *            the highlights to set
	 */
	public void setHighlights(String highlights)
	{
		this.highlights = highlights;
	}

	/**
	 * @return the offerForNumberOfPeople
	 */
	public int getOfferForNumberOfPeople()
	{
		return offerForNumberOfPeople;
	}

	/**
	 * @param offerForNumberOfPeople
	 *            the offerForNumberOfPeople to set
	 */
	public void setOfferForNumberOfPeople(int offerForNumberOfPeople)
	{
		this.offerForNumberOfPeople = offerForNumberOfPeople;
	}

	/**
	 * @return the isValid
	 */
	public boolean isValid()
	{
		return isValid;
	}

	/**
	 * @param isValid
	 *            the isValid to set
	 */
	public void setValid(boolean isValid)
	{
		this.isValid = isValid;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the offerStartTimestamp
	 */
	public Timestamp getOfferStartTimestamp()
	{
		return offerStartTimestamp;
	}

	/**
	 * @param offerStartTimestamp
	 *            the offerStartTimestamp to set
	 */
	public void setOfferStartTimestamp(Timestamp offerStartTimestamp)
	{
		this.offerStartTimestamp = offerStartTimestamp;
	}

	/**
	 * @return the offerFinishTimestamp
	 */
	public Timestamp getOfferFinishTimestamp()
	{
		return offerFinishTimestamp;
	}

	/**
	 * @param offerFinishTimestamp
	 *            the offerFinishTimestamp to set
	 */
	public void setOfferFinishTimestamp(Timestamp offerFinishTimestamp)
	{
		this.offerFinishTimestamp = offerFinishTimestamp;
	}

}
