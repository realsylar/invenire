/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * This is a basic status of the dish in the restaurant. Although the dish has
 * the is_active flag to tell if the dish is present or not, this gives a wider
 * range of options to have an actual understanding of the dish status. Can be
 * updated at the runtime as well.
 *
 * @author Anurag Agrawal
 */
public enum DishStatus
{
	AVAILABLE(1), NOTAVAILABLE(2), DISCONTINUED(3), TEMPORARILYDISCONTINUED(4);

	private final int	value;

	/**
	 * @param value
	 */
	private DishStatus(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

}
