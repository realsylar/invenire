/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * @author Anurag Agrawal
 *
 */
public enum CampaignTypes
{

	EXTRABITE(0), FACEBOOK(1), EMAIL(3), SMS(4), ALL_SOCIAL_MEDIA(5);

	private int	value;

	/**
	 * @param value
	 */
	private CampaignTypes(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

	public static CampaignTypes getEnumForValue(int value)
	{
		for (CampaignTypes type : values())
		{
			if (type.getValue() == value)
			{
				return type;
			}
		}
		return null;
	}

	public static String getEnumNameForValue(int value)
	{
		for (CampaignTypes type : values())
		{
			if (type.getValue() == value)
			{
				return type.name();
			}
		}
		return null;
	}
}
