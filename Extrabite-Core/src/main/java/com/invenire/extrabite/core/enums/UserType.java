/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * This table is to define the type of user that is stored in the table. This
 * can be a standard user or a restaurant account user.
 *
 * @author Anurag Agrawal
 */
public enum UserType
{

	STANDARD(1), RESTAURANTADMIN(2), RESTAURANTMANAGER(3), RESTAURANTOWNER(4);

	private int	value;

	/**
	 * @param value
	 */
	private UserType(int value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}

}
