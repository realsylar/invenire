/**
 *
 */
package com.invenire.extrabite.core.persistance.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "client_license_subscription_details")
public class ClientLicenseSubscriptionDetails
{

	@Id
	@Column(name = "subscription_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int			subscriptionId;

	@Column(name = "client_id")
	private int			clientId;

	@Column(name = "subscription_activated_timestamp")
	private Timestamp	subscriptionActivatedTimestamp;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "license_id")
	private LicenseInfo	license;

	@Column(name = "is_active")
	private boolean		isActive;

	@Column(name = "payment_status")
	private int			paymentStatus;

	/**
	 * @return the subscriptionId
	 */
	public int getSubscriptionId()
	{
		return subscriptionId;
	}

	/**
	 * @param subscriptionId
	 *            the subscriptionId to set
	 */
	public void setSubscriptionId(int subscriptionId)
	{
		this.subscriptionId = subscriptionId;
	}

	/**
	 * @return the clientId
	 */
	public int getClientId()
	{
		return clientId;
	}

	/**
	 * @param clientId
	 *            the clientId to set
	 */
	public void setClientId(int clientId)
	{
		this.clientId = clientId;
	}

	/**
	 * @return the subscriptionActivatedTimestamp
	 */
	public Timestamp getSubscriptionActivatedTimestamp()
	{
		return subscriptionActivatedTimestamp;
	}

	/**
	 * @param subscriptionActivatedTimestamp
	 *            the subscriptionActivatedTimestamp to set
	 */
	public void setSubscriptionActivatedTimestamp(
			Timestamp subscriptionActivatedTimestamp)
	{
		this.subscriptionActivatedTimestamp = subscriptionActivatedTimestamp;
	}

	/**
	 * @return the licenseId
	 */
	public LicenseInfo getLicense()
	{
		return license;
	}

	/**
	 * @param licenseId
	 *            the licenseId to set
	 */
	public void setLicense(LicenseInfo license)
	{
		this.license = license;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the paymentStatus
	 */
	public int getPaymentStatus()
	{
		return paymentStatus;
	}

	/**
	 * @param paymentStatus
	 *            the paymentStatus to set
	 */
	public void setPaymentStatus(int paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}

}
