/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * A list of out of box standardized menu categories to ensure faster
 * configurations
 * 
 * @author Anurag Agrawal
 */
public enum OutOfBoxMenuCategories
{

}
