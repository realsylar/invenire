/**
 *
 */
package com.invenire.extrabite.core.dto;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * @author Anurag Agrawal
 *
 */
public class ClientLicenseSubscriptionDetailsDTO
{

	private int		clientId;
	private String	subscriptionActivatedTimestamp;
	private int		licenseId;
	private String	licenseDisplayName;
	private String	licenseDescription;
	private String	isActive;
	private String	paymentStatus;

	/**
	 * @param clientId
	 * @param subscriptionActivatedTimestamp
	 * @param licenseId
	 * @param licenseDisplayName
	 * @param licenseDescription
	 * @param isActive
	 * @param paymentStatus
	 */
	public ClientLicenseSubscriptionDetailsDTO(int clientId,
			Timestamp subscriptionActivatedTimestamp, int licenseId,
			String licenseDisplayName, String licenseDescription,
			boolean isActive, String paymentStatus)
	{
		super();
		this.clientId = clientId;
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		this.subscriptionActivatedTimestamp = formatter
				.format(subscriptionActivatedTimestamp);
		this.licenseId = licenseId;
		this.licenseDisplayName = licenseDisplayName;
		this.licenseDescription = licenseDescription;
		if (isActive)
			this.isActive = "YES";
		else
			this.isActive = "NO";
		this.paymentStatus = paymentStatus;
	}

	/**
	 * @return the clientId
	 */
	public int getClientId()
	{
		return clientId;
	}

	/**
	 * @param clientId
	 *            the clientId to set
	 */
	public void setClientId(int clientId)
	{
		this.clientId = clientId;
	}

	/**
	 * @return the subscriptionActivatedTimestamp
	 */
	public String getSubscriptionActivatedTimestamp()
	{
		return subscriptionActivatedTimestamp;
	}

	/**
	 * @param subscriptionActivatedTimestamp
	 *            the subscriptionActivatedTimestamp to set
	 */
	public void setSubscriptionActivatedTimestamp(
			String subscriptionActivatedTimestamp)
	{
		this.subscriptionActivatedTimestamp = subscriptionActivatedTimestamp;
	}

	/**
	 * @return the licenseId
	 */
	public int getLicenseId()
	{
		return licenseId;
	}

	/**
	 * @param licenseId
	 *            the licenseId to set
	 */
	public void setLicenseId(int licenseId)
	{
		this.licenseId = licenseId;
	}

	/**
	 * @return the licenseDisplayName
	 */
	public String getLicenseDisplayName()
	{
		return licenseDisplayName;
	}

	/**
	 * @param licenseDisplayName
	 *            the licenseDisplayName to set
	 */
	public void setLicenseDisplayName(String licenseDisplayName)
	{
		this.licenseDisplayName = licenseDisplayName;
	}

	/**
	 * @return the licenseDescription
	 */
	public String getLicenseDescription()
	{
		return licenseDescription;
	}

	/**
	 * @param licenseDescription
	 *            the licenseDescription to set
	 */
	public void setLicenseDescription(String licenseDescription)
	{
		this.licenseDescription = licenseDescription;
	}

	/**
	 * @return the isActive
	 */
	public String isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(String isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the paymentStatus
	 */
	public String getPaymentStatus()
	{
		return paymentStatus;
	}

	/**
	 * @param paymentStatus
	 *            the paymentStatus to set
	 */
	public void setPaymentStatus(String paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "ClientLicenseSubscriptionDetailsDTO [clientId=" + clientId
				+ ", subscriptionActivatedTimestamp="
				+ subscriptionActivatedTimestamp + ", licenseId=" + licenseId
				+ ", licenseDisplayName=" + licenseDisplayName
				+ ", licenseDescription=" + licenseDescription + ", isActive="
				+ isActive + ", paymentStatus=" + paymentStatus + "]";
	}

}
