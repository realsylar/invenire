/**
 *
 */
package com.invenire.extrabite.core.persistance.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.core.persistance.entity.ClientToUserMapping;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ClientToUserMappingDAO
{

	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public ClientToUserMappingDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public List<ClientToUserMapping> loadClientUserMappingFromDB()
	{
		Session sess = sessionFactory.getCurrentSession();
		List<ClientToUserMapping> list = sess.createCriteria(
				ClientToUserMapping.class).list();
		return list;
	}
}
