/**
 *
 */
package com.invenire.extrabite.core.persistance.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "license_info")
public class LicenseInfo
{

	@Id
	@Column(name = "license_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int			licenseId;

	@Column(name = "license_disp_name")
	private String		licenseDisplayName;

	@Column(name = "license_description")
	private String		licenseDescription;

	@Column(name = "license_validity_type")
	private int			validityTypeMultiplier;

	@Column(name = "license_billing_type")
	private float		billingTypeMultiplier;

	@Column(name = "license_unit_price")
	private int			licenseUnitPrice;

	@Column(name = "creation_timestamp")
	private Timestamp	creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp	modificationTimestamp;

	@Column(name = "is_active")
	private boolean		isActive;

	@Column(name = "allowed_user_number")
	private int			allowedUsers;

	/**
	 * @return the licenseId
	 */
	public int getLicenseId()
	{
		return licenseId;
	}

	/**
	 * @param licenseId
	 *            the licenseId to set
	 */
	public void setLicenseId(int licenseId)
	{
		this.licenseId = licenseId;
	}

	/**
	 * @return the licenseDisplayName
	 */
	public String getLicenseDisplayName()
	{
		return licenseDisplayName;
	}

	/**
	 * @param licenseDisplayName
	 *            the licenseDisplayName to set
	 */
	public void setLicenseDisplayName(String licenseDisplayName)
	{
		this.licenseDisplayName = licenseDisplayName;
	}

	/**
	 * @return the licenseDescription
	 */
	public String getLicenseDescription()
	{
		return licenseDescription;
	}

	/**
	 * @param licenseDescription
	 *            the licenseDescription to set
	 */
	public void setLicenseDescription(String licenseDescription)
	{
		this.licenseDescription = licenseDescription;
	}

	/**
	 * @return the licenseValidityType
	 */
	public int getValidityTypeMultiplier()
	{
		return validityTypeMultiplier;
	}

	/**
	 * @param licenseValidityType
	 *            the licenseValidityType to set
	 */
	public void setValidityTypeMultiplier(int validityTypeMultiplier)
	{
		this.validityTypeMultiplier = validityTypeMultiplier;
	}

	/**
	 * @return the licenseUnitPrice
	 */
	public int getLicenseUnitPrice()
	{
		return licenseUnitPrice;
	}

	/**
	 * @param licenseUnitPrice
	 *            the licenseUnitPrice to set
	 */
	public void setLicenseUnitPrice(int licenseUnitPrice)
	{
		this.licenseUnitPrice = licenseUnitPrice;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the allowedUsers
	 */
	public int getAllowedUsers()
	{
		return allowedUsers;
	}

	/**
	 * @param allowedUsers
	 *            the allowedUsers to set
	 */
	public void setAllowedUsers(int allowedUsers)
	{
		this.allowedUsers = allowedUsers;
	}

	/**
	 * @return the billingTypeMultiplier
	 */
	public float getBillingTypeMultiplier()
	{
		return billingTypeMultiplier;
	}

	/**
	 * @param billingTypeMultiplier
	 *            the billingTypeMultiplier to set
	 */
	public void setBillingTypeMultiplier(float billingTypeMultiplier)
	{
		this.billingTypeMultiplier = billingTypeMultiplier;
	}

}
