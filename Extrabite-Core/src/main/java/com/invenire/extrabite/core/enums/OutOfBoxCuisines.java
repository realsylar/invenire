/**
 *
 */
package com.invenire.extrabite.core.enums;

/**
 * A list of out of box standardized cuisines to ensure faster configurations
 *
 * @author Anurag Agrawal
 */
public enum OutOfBoxCuisines
{

}
