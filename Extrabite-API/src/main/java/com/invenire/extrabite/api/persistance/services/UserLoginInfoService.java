/**
 *
 */
package com.invenire.extrabite.api.persistance.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.api.persistence.dao.gen.UserLoginInfoDAO;

/**
 * Service class for all functionalities on the User Login Info entity
 *
 * @author Anurag Agrawal
 */
@Service
public class UserLoginInfoService
{

	@SuppressWarnings("unused")
	private UserLoginInfoDAO	userLoginInfoDAO;

	/**
	 * Autowired constructor to automatically load the userLoginInfoDAO object
	 * from the spring - hibernate configuration.
	 *
	 * @param userLoginInfoDAO
	 */
	@Autowired
	public UserLoginInfoService(UserLoginInfoDAO userLoginInfoDAO)
	{
		super();
		this.userLoginInfoDAO = userLoginInfoDAO;
	}

}
