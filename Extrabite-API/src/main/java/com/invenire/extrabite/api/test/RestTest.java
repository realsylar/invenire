/**
 *
 */
package com.invenire.extrabite.api.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

/**
 * @author Anurag Agrawal
 *
 */
@Component
@Path("/test")
public class RestTest
{

	@GET
	@Path("/status")
	public Response savePayment()
	{

		return Response.status(200).entity("Test Success").build();

	}

}
