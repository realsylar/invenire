package com.invenire.extrabite.api.config;

import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author Anurag Agrawal
 *
 */
public class ExtrabiteAPIJerseyConfig extends ResourceConfig
{

	public ExtrabiteAPIJerseyConfig()
	{
		packages("com.invenire.extrabite.api");
	}
}