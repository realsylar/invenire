/**
 *
 */
package com.invenire.extrabite.api.persistence.entity.gen;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * DB Entity class for managing user personal details
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "user_personal_info")
public class UserPersonalInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_personal_info_id")
	private int		userPersonalInfoId;

	@Column(name = "user_id")
	private int		userId;

	@Column(name = "current_city")
	private String	currentCity;

	@Column(name = "relationship_status")
	private String	relationshipStatus;

	@Column(name = "dob")
	private Date	dob;

	@Column(name = "education")
	private String	education;

	@Column(name = "current_workplace")
	private String	currentWorkplace;

	@Column(name = "likes")
	private String	likes;

	@Column(name = "sports")
	private String	sports;

	@Column(name = "music")
	private String	music;

	@Column(name = "movies")
	private String	movies;

	@Column(name = "tv_shows")
	private String	tvShows;

	@Column(name = "books")
	private String	books;

	/**
	 * @return the userPersonalInfoId
	 */
	public int getUserPersonalInfoId()
	{
		return userPersonalInfoId;
	}

	/**
	 * @param userPersonalInfoId
	 *            the userPersonalInfoId to set
	 */
	public void setUserPersonalInfoId(int userPersonalInfoId)
	{
		this.userPersonalInfoId = userPersonalInfoId;
	}

	/**
	 * @return the currentCity
	 */
	public String getCurrentCity()
	{
		return currentCity;
	}

	/**
	 * @param currentCity
	 *            the currentCity to set
	 */
	public void setCurrentCity(String currentCity)
	{
		this.currentCity = currentCity;
	}

	/**
	 * @return the relationshipStatus
	 */
	public String getRelationshipStatus()
	{
		return relationshipStatus;
	}

	/**
	 * @param relationshipStatus
	 *            the relationshipStatus to set
	 */
	public void setRelationshipStatus(String relationshipStatus)
	{
		this.relationshipStatus = relationshipStatus;
	}

	/**
	 * @return the dob
	 */
	public Date getDob()
	{
		return dob;
	}

	/**
	 * @param dob
	 *            the dob to set
	 */
	public void setDob(Date dob)
	{
		this.dob = dob;
	}

	/**
	 * @return the education
	 */
	public String getEducation()
	{
		return education;
	}

	/**
	 * @param education
	 *            the education to set
	 */
	public void setEducation(String education)
	{
		this.education = education;
	}

	/**
	 * @return the currentWorkplace
	 */
	public String getCurrentWorkplace()
	{
		return currentWorkplace;
	}

	/**
	 * @param currentWorkplace
	 *            the currentWorkplace to set
	 */
	public void setCurrentWorkplace(String currentWorkplace)
	{
		this.currentWorkplace = currentWorkplace;
	}

	/**
	 * @return the likes
	 */
	public String getLikes()
	{
		return likes;
	}

	/**
	 * @param likes
	 *            the likes to set
	 */
	public void setLikes(String likes)
	{
		this.likes = likes;
	}

	/**
	 * @return the sports
	 */
	public String getSports()
	{
		return sports;
	}

	/**
	 * @param sports
	 *            the sports to set
	 */
	public void setSports(String sports)
	{
		this.sports = sports;
	}

	/**
	 * @return the music
	 */
	public String getMusic()
	{
		return music;
	}

	/**
	 * @param music
	 *            the music to set
	 */
	public void setMusic(String music)
	{
		this.music = music;
	}

	/**
	 * @return the movies
	 */
	public String getMovies()
	{
		return movies;
	}

	/**
	 * @param movies
	 *            the movies to set
	 */
	public void setMovies(String movies)
	{
		this.movies = movies;
	}

	/**
	 * @return the tvShows
	 */
	public String getTvShows()
	{
		return tvShows;
	}

	/**
	 * @param tvShows
	 *            the tvShows to set
	 */
	public void setTvShows(String tvShows)
	{
		this.tvShows = tvShows;
	}

	/**
	 * @return the books
	 */
	public String getBooks()
	{
		return books;
	}

	/**
	 * @param books
	 *            the books to set
	 */
	public void setBooks(String books)
	{
		this.books = books;
	}

	/**
	 * @return the userId
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

}
