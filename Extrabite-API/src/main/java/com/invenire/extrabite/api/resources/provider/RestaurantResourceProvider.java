/**
 *
 */
package com.invenire.extrabite.api.resources.provider;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.api.persistance.services.RestaurantInfoService;
import com.invenire.extrabite.core.dto.RestaurantInfoDTO;
import com.invenire.extrabite.core.persistance.entity.RestaurantInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class RestaurantResourceProvider
{

	private RestaurantInfoService	restaurantInfoService;
	private DozerBeanMapper			dozerBeanMapper;

	/**
	 * @param restaurantService
	 * @param dozerBeanMapper
	 */
	@Autowired
	public RestaurantResourceProvider(
			RestaurantInfoService restaurantInfoService,
			DozerBeanMapper dozerBeanMapper)
	{
		super();
		this.restaurantInfoService = restaurantInfoService;
		this.dozerBeanMapper = dozerBeanMapper;
	}

	/**
	 * @param id
	 * @return
	 */
	@Transactional(readOnly = true)
	public RestaurantInfoDTO getRestaurantById(int id)
	{
		RestaurantInfo rest = restaurantInfoService.getRestaurantById(id);
		RestaurantInfoDTO restDTO = rest == null ? null : dozerBeanMapper.map(
				rest, RestaurantInfoDTO.class);
		return restDTO;
	}
}
