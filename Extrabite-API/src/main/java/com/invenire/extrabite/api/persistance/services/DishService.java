/**
 *
 */
package com.invenire.extrabite.api.persistance.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.core.persistance.dao.DishDAO;

/**
 * Service class for all functionalities on the Dish entity
 *
 * @author Anurag Agrawal
 */
@Service
public class DishService
{

	@SuppressWarnings("unused")
	private DishDAO	dishDAO;

	/**
	 * Autowired constructor to automatically load the dishDAO object from the
	 * spring - hibernate configuration.
	 *
	 * @param dishDAO
	 */
	@Autowired
	public DishService(DishDAO dishDAO)
	{
		super();
		this.dishDAO = dishDAO;
	}

}
