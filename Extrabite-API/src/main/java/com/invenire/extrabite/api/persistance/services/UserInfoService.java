/**
 *
 */
package com.invenire.extrabite.api.persistance.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.api.persistence.dao.gen.UserInfoDAO;
import com.invenire.extrabite.api.persistence.entity.gen.UserInfo;

/**
 * Service class for all functionalities on the User Info entity
 *
 * @author Anurag Agrawal
 */
@Service
public class UserInfoService
{

	private UserInfoDAO	userInfoDAO;

	/**
	 * Autowired constructor to automatically load the userInfoDAO object from
	 * the spring - hibernate configuration.
	 *
	 * @param userInfoDAO
	 */
	@Autowired
	public UserInfoService(UserInfoDAO userInfoDAO)
	{
		super();
		this.userInfoDAO = userInfoDAO;
	}

	/**
	 * Service method to load the user info object from DB based on the
	 * identifier i.e. table primary key
	 *
	 * @param userId
	 *            The id of the user whose information has to be fetched
	 * @return The UserInfo object pertaining to the id. Null if not present
	 */
	@Transactional(readOnly = true)
	public UserInfo getUser(int userId)
	{
		return userInfoDAO.getUser(userId);
	}

	/**
	 * Service method to load the user info object from DB based on the email of
	 * the user
	 *
	 * @param email
	 *            The email id of the user whose information has to be fetched
	 * @return The UserInfo object pertaining to the email id. Null if not
	 *         present
	 */
	@Transactional(readOnly = true)
	public UserInfo getUserByEmail(String email)
	{
		return userInfoDAO.getUserByEmail(email);
	}

	/**
	 * Service method to load the user info object from DB based on the user
	 * name of the user
	 *
	 * @param userName
	 *            The user name of the user whose information has to be fetched
	 * @return The UserInfo object pertaining to the user name. Null if not
	 *         present
	 */
	@Transactional(readOnly = true)
	public UserInfo getUserByUserName(String userName)
	{
		return userInfoDAO.getUserByUserName(userName);
	}

	/**
	 * Service method to load the user info object from DB based on the mobile
	 * phone number of the user
	 *
	 * @param phoneNumber
	 *            The phone number of the user whose information has to be
	 *            fetched
	 * @return The UserInfo object pertaining to the phone number. Null if not
	 *         present
	 */
	@Transactional(readOnly = true)
	public UserInfo getUserByPhoneNumber(String phoneNumber)
	{
		return userInfoDAO.getUserByPhoneNumber(phoneNumber);
	}

	/**
	 * Service method to persist the UserInfo object
	 *
	 * @param user
	 *            The UserInfo object to be persisted
	 * @return The integer user id value used to identify the persisted user
	 *         here on
	 */
	public int saveUserInfo(UserInfo user)
	{
		return userInfoDAO.saveUserInfo(user);
	}

	/**
	 * Service method to update an already saved user or persist a new user if
	 * no user already saved
	 *
	 * @param user
	 *            The user object to be updated or persisted
	 */
	public void saveOrUpdateUserInfo(UserInfo user)
	{
		userInfoDAO.saveOrUpdateUserInfo(user);
	}
}
