/**
 *
 */
package com.invenire.extrabite.api.persistance.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.core.persistance.dao.DishDisplayInfoDAO;

/**
 * Service class for all functionalities on the Dish entity
 *
 * @author Anurag Agrawal
 */
@Service
public class DishDisplayInfoService
{

	@SuppressWarnings("unused")
	private DishDisplayInfoDAO	dishDisplayInfoDAO;

	/**
	 * Autowired constructor to automatically load the dishDisplayInfoDAO object
	 * from the spring - hibernate configuration.
	 *
	 * @param dishDisplayInfoDAO
	 */
	@Autowired
	public DishDisplayInfoService(DishDisplayInfoDAO dishDisplayInfoDAO)
	{
		super();
		this.dishDisplayInfoDAO = dishDisplayInfoDAO;
	}

}
