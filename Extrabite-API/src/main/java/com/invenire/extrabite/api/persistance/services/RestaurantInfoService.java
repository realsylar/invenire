/**
 *
 */
package com.invenire.extrabite.api.persistance.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.core.persistance.dao.RestaurantInfoDAO;
import com.invenire.extrabite.core.persistance.entity.RestaurantInfo;

/**
 * Service class for all functionalities on the Restaurant Info entity
 *
 * @author Anurag Agrawal
 */
@Service
public class RestaurantInfoService
{

	private RestaurantInfoDAO	restaurantInfoDAO;

	/**
	 * Autowired constructor to automatically load the restaurantInfoDAO object
	 * from the spring - hibernate configuration.
	 *
	 * @param restaurantInfoDAO
	 */
	@Autowired
	public RestaurantInfoService(RestaurantInfoDAO restaurantInfoDAO)
	{
		super();
		this.restaurantInfoDAO = restaurantInfoDAO;
	}

	/**
	 * A method to get the complete restaurant details by the id of the
	 * restaurant.
	 *
	 * @param id
	 *            The unique identifier for the restaurant.
	 * @return The complete restaurant info object
	 */
	@Transactional(readOnly = true)
	public RestaurantInfo getRestaurantById(int restaurantInfoId)
	{
		return restaurantInfoDAO.getRestaurantById(restaurantInfoId);
	}

	/**
	 * A method to get the complete restaurant details of all the restaurants
	 * owned by a client by the id of the client.
	 *
	 * @param clientId
	 *            The unique identifier for the client
	 * @return The list of complete restaurant info objects
	 */
	@Transactional(readOnly = true)
	public List<RestaurantInfo> getRestaurantsByClientId(int clientId)
	{
		return restaurantInfoDAO.getRestaurantsByClientId(clientId);
	}

	/**
	 * A method to save the restaurant details in DB
	 *
	 * @param restaurantInfo
	 *            The restaurant info details object to be persisted
	 * @return The unique identifier id of the restaurant being persisted
	 */
	public int saveRestaurant(RestaurantInfo restaurantInfo)
	{
		return restaurantInfoDAO.saveRestaurant(restaurantInfo);
	}

	/**
	 * A method to save or update the restaurant details in DB
	 *
	 * @param restaurantInfo
	 *            The restaurant info details object to be persisted or update
	 */
	public void saveOrUpdateRestaurant(RestaurantInfo restaurantInfo)
	{
		restaurantInfoDAO.saveOrUpdateRestaurant(restaurantInfo);
	}
}
