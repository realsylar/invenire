/**
 *
 */
package com.invenire.extrabite.api.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.invenire.extrabite.api.resources.provider.RestaurantResourceProvider;
import com.invenire.extrabite.core.dto.RestaurantInfoDTO;

/**
 * @author Anurag Agrawal
 *
 */
@Component
@Path("/restnt")
public class RestaurantResource
{

	private RestaurantResourceProvider	restaurantResourceProvider;

	/**
	 * @param restaurantService
	 */
	@Autowired
	public RestaurantResource(
			RestaurantResourceProvider restaurantResourceProvider)
	{
		super();
		this.restaurantResourceProvider = restaurantResourceProvider;
	}

	@GET
	@Path("{id}")
	@Produces(value = {MediaType.APPLICATION_JSON})
	public RestaurantInfoDTO getRestaurantDetails(@PathParam("id") int id)
	{
		return restaurantResourceProvider.getRestaurantById(id);
	}
}
