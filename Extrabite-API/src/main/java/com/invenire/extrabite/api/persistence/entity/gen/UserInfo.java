/**
 *
 */
package com.invenire.extrabite.api.persistence.entity.gen;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.invenire.extrabite.core.persistance.entity.ContactInfo;

/**
 * DB Entity class for managing user details
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "user_info")
public class UserInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private int				userId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contact_info_id")
	private ContactInfo		contactInfo;

	@Column(name = "email")
	private String			email;

	@Column(name = "creation_timestamp")
	private Timestamp		creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp		modificationTimestamp;

	@Column(name = "is_active")
	private boolean			isActive;

	@Column(name = "user_type")
	private int				userType;

	@OneToOne
	@JoinColumn(name = "user_login_id")
	private UserLoginInfo	loginInfo;

	/**
	 * @return the userId
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * @return the contactInfo
	 */
	public ContactInfo getContactInfo()
	{
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(ContactInfo contactInfo)
	{
		this.contactInfo = contactInfo;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive()
	{
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive)
	{
		this.isActive = isActive;
	}

	/**
	 * @return the userType
	 */
	public int getUserType()
	{
		return userType;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(int userType)
	{
		this.userType = userType;
	}

	/**
	 * @return the loginInfo
	 */
	public UserLoginInfo getLoginInfo()
	{
		return loginInfo;
	}

	/**
	 * @param loginInfo
	 *            the loginInfo to set
	 */
	public void setLoginInfo(UserLoginInfo loginInfo)
	{
		this.loginInfo = loginInfo;
	}

}
