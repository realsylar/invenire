/**
 *
 */
package com.invenire.extrabite.api.persistance.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.core.persistance.dao.ClientInfoDAO;
import com.invenire.extrabite.core.persistance.entity.ClientInfo;

/**
 * Service class for all functionalities on the Client entity
 *
 * @author Anurag Agrawal
 */
@Service
public class ClientInfoService
{

	private ClientInfoDAO	clientInfoDAO;

	/**
	 * Autowired constructor to automatically load the clientInfoDAO object from
	 * the spring - hibernate configuration.
	 *
	 * @param clientInfoDAO
	 */
	@Autowired
	public ClientInfoService(ClientInfoDAO clientInfoDAO)
	{
		super();
		this.clientInfoDAO = clientInfoDAO;
	}

	/**
	 * A method to save the client info object to DB.
	 *
	 * @param clientInfo
	 *            THe client info object to be saved
	 * @return the identifier of the new object created in DB.
	 */
	public int saveClientInfo(ClientInfo clientInfo)
	{
		return clientInfoDAO.saveClientInfo(clientInfo);
	}

	/**
	 * Save or update method for the Client Info objects. This will update any
	 * persisted object and will create a new object for the one's which are not
	 * already persisted
	 *
	 * @param clientInfo
	 *            The client info object to be saved or updated.
	 */
	public void saveOrUpdateClientInfo(ClientInfo clientInfo)
	{
		clientInfoDAO.saveOrUpdateClientInfo(clientInfo);
	}

	/**
	 * To read the client info object present in the DB using the unique id
	 * value.
	 *
	 * @param clientInfoId
	 *            The unique id corresponding to the client to be looked up
	 * @return The client info object pertaining to the unique id. Null if not
	 *         found in DB.
	 */
	@Transactional(readOnly = true)
	public ClientInfo getClientInfoById(int clientInfoId)
	{
		return clientInfoDAO.getClientInfoById(clientInfoId);
	}

	/**
	 * To read the client info object present in the DB using the unique email
	 * id of the user.
	 *
	 * @param email
	 *            The unique email id corresponding to the client to be looked
	 *            up
	 * @return The client info object pertaining to the unique email id. Null if
	 *         not found in DB.
	 */
	@Transactional(readOnly = true)
	public ClientInfo getClientInfoByEmail(String email)
	{
		return clientInfoDAO.getClientInfoByEmail(email);
	}

	/**
	 * To read the client info object present in the DB using the unique id
	 * value and the isActive flag.
	 *
	 * @param clientInfoId
	 *            The unique id corresponding to the client to be looked up
	 * @param isActive
	 *            The value of the flag which can be used to get specific
	 *            selection according to the flag value
	 * @return The client info object pertaining to the unique id and the
	 *         isActive flag. Null if not found in DB.
	 */
	@Transactional(readOnly = true)
	public ClientInfo getActiveClientInfoById(int clientInfoId, boolean isActive)
	{
		return clientInfoDAO.getActiveClientInfoById(clientInfoId, isActive);
	}

	/**
	 * To read the client info object present in the DB using the unique email
	 * id of the user and the isActive flag.
	 *
	 * @param email
	 *            The unique email id corresponding to the client to be looked
	 *            up
	 * @param isActive
	 *            The value of the flag which can be used to get specific
	 *            selection according to the flag value
	 * @return The client info object pertaining to the unique email id and the
	 *         isActive flag. Null if not found in DB.
	 */
	@Transactional(readOnly = true)
	public ClientInfo getActiveClientInfoByEmail(String email)
	{
		return clientInfoDAO.getActiveClientInfoByEmail(email);
	}
}
