# Invenire
The complete code base for Invenire products. Currently consists Invenire commons (Jar build), Extrabite commons (Jar build) and Extrabite API (war build) and the Extrabite Web (war build).

Created using eclipse Luna as the IDE and maven as build too.
Versions:
Apache Maven 3.0.5
Java version: 1.7.0_76, vendor: Oracle Corporation
