<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
</head>
<body>
	<h2>Product Pricing</h2>
	<p>${successMsg}</p>
	<form:form method="POST" commandName="contactSalesFormData"
		action="submit">
		<form:errors label="techDifficulty" cssClass="errorblock"
			element="div" />
		<table align="center" >
			<tr>
				<td>Name :</td>
				<td><form:input path="userName" /></td>
				<td><form:errors path="userName" cssClass="error" /></td>
			</tr>
			<tr />
			<tr>
				<td>Email Address :</td>
				<td><form:input path="email" /></td>
				<td><form:errors path="email" cssClass="error" /></td>
			</tr>
			<tr />
			<tr>
				<td>Phone Number :</td>
				<td><form:input path="phNo" /></td>
				<td><form:errors path="phNo" cssClass="error" /></td>
			</tr>
			<tr />
			<tr>
				<td>City :</td>
				<td><form:select path="city">
						<form:options items="${cityOptions}"></form:options>
					</form:select></td>
				<td><form:errors path="city" cssClass="error" /></td>
			</tr>
			<tr />
			<tr>
				<td>Specific query :</td>
				<td><form:textarea path="query" /></td>
				<td><form:errors path="query" cssClass="error" /></td>
			</tr>
			<tr />
			<tr>
				<td colspan="3"><input type="submit" /></td>
			</tr>
		</table>
	</form:form>

</body>
</html>