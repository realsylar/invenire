<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}

.td {
	text-align: center;
}
​
</style>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-2.1.4.min.js">
	
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#home").trigger('click');
	});
	function loadHome() {
		$.ajax({
			url : "/extrabiteweb/user/userDetails",
			type : 'GET',
			cache : false,
			beforeSend : function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success : function(resp) {
				$("div.home_details").html(
						"<p> Welcome to your dashboard "
								+ resp.loginInfoDTO.userName + "</p>");
			}
		});
		return true;
	}
	function loadSubsDetails() {
		$.ajax({
			url : "/extrabiteweb/subs/subsDetails",
			type : 'GET',
			cache : false,
			beforeSend : function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success : function(resp) {
				$("div.home_details").html(
						"<table>" + "<tr><td>" + "License details"
								+ "</td><td>" + "Activated" + "</td><td>"
								+ "Activation Time" + "</td><td>"
								+ "Payment status" + "</td></tr>" + "<tr><td>"
								+ resp.licenseDisplayName + "</td><td>"
								+ resp.isActive + "</td><td>"
								+ resp.subscriptionActivatedTimestamp
								+ "</td><td>" + resp.paymentStatus
								+ "</td></tr><tr><td>"
								+ resp.licenseDescription + "</td><td></td><td></td><td><a href=\"/extrabiteweb/dashboard/subsEdit\"></a></td></tr>"
								+ "</table>");
			}
		});
		return true;
	}
	function loadOffersDetails() {
		$.ajax({
			url : "/extrabiteweb/offers/offerDetails",
			type : 'GET',
			cache : false,
			beforeSend : function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success : function(resp) {
				$("div.home_details").html(
						"<p> Welcome to your offers page "
								+ resp + "</p>");
			}
		});
		return true;
	}
</script>
<title>Dashboard</title>
</head>
<body>
	<table width="100%">
		<tr>
			<td width="25%"><img src="/resources/image/logo.png"></img></td>
		</tr>
		<tr>
			<td width="25%" class="td"><button id="home"
					onclick="loadHome()">Home</button></td>
			<td width="75%" class="td">
				<div class="home_details"></div>
			</td>
		</tr>
		<tr>
			<td width="25%" class="td"><button onclick="loadSubsDetails()">Subscription
					Details</button></td>
		</tr>
		<tr>
			<td width="25%" class="td"><button onclick="loadOffersDetails()">Offer 
					Management</button></td>
		</tr>
	</table>
</body>
</html>
