<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
<title>Client Login Page</title>
</head>
<body>
	<form:form commandName="loginDetail" method="POST" action="auth"> 
			<form:errors label="techDifficulty" cssClass="errorblock"
			element="div" />
			<form:errors label="loginErrors" cssClass="errorblock"
			element="div" />
		<table align="center" >
			<tr>
				<td>Email :</td>
				<td><form:input path="email" /></td>
				<td><form:errors path="email" cssClass="error" /></td>
			</tr>
			<tr />
			<tr>
				<td>Password :</td>
				<td><form:password path="password" /></td>
				<td><form:errors path="password" cssClass="error" /></td>
			</tr>
			<tr />
			<tr>
				<td>Remember me :</td>
				<td><form:checkbox path="rememberChecked"></form:checkbox></td>
				<td><form:errors path="rememberChecked" cssClass="error" /></td>
			</tr>
			<tr />
			<tr>
				<td colspan="3"><input type="submit" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>
