/**
 *
 */
package com.invenire.extrabite.web.persistence.service.sec;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.web.persistence.dao.sec.ExtrabiteUserRolesDAO;

/**
 * Service class for all DB operations on the user_roles table.
 *
 * @author Anurag Agrawal
 */
@Service
public class ExtrabiteUserRolesService
{

	private ExtrabiteUserRolesDAO	dao;

	/**
	 * Autowired constructor to automatically load the dao object from the
	 * spring - hibernate configuration.
	 *
	 * @param dao
	 */
	@Autowired
	public ExtrabiteUserRolesService(ExtrabiteUserRolesDAO dao)
	{
		super();
		this.dao = dao;
	}

	@Transactional(readOnly = true)
	public List<Integer> getAllRolesForUser(int userId)
	{
		return dao.getAllRolesForUser(userId);
	}
}
