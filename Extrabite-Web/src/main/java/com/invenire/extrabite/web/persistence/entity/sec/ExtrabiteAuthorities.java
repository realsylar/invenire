/**
 *
 */
package com.invenire.extrabite.web.persistence.entity.sec;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author Anurag Agrawal
 *
 */
@Entity
@Table(name = "authorities")
public class ExtrabiteAuthorities implements GrantedAuthority
{

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int					id;
	@Column(name = "authority")
	private String				authority;

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @param authority
	 *            the authority to set
	 */
	public void setAuthority(String authority)
	{
		this.authority = authority;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.core.GrantedAuthority#getAuthority()
	 */
	@Override
	public String getAuthority()
	{
		return authority;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "ExtrabiteAuthorities [id=" + id + ", authority=" + authority
				+ "]";
	}

}
