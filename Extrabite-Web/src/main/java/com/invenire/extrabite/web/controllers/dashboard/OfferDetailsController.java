/**
 *
 */
package com.invenire.extrabite.web.controllers.dashboard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.invenire.extrabite.web.persistence.entity.gen.UserInfo;
import com.invenire.extrabite.web.sess.ExtrabiteWebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "offers")
public class OfferDetailsController
{

	private static final Logger			logger	= Logger.getLogger(OfferDetailsController.class);

	private ExtrabiteWebSessionManager	webSessionManager;

	/**
	 * @param webSessionManager
	 */
	@Autowired
	public OfferDetailsController(ExtrabiteWebSessionManager webSessionManager)
	{
		super();
		this.webSessionManager = webSessionManager;
	}

	@RequestMapping(method = RequestMethod.GET, value = "offerDetails")
	@ResponseBody
	public String getUserDetails(HttpServletRequest request,
			HttpServletResponse response)
	{
		UserInfo loggedInUser = webSessionManager.getUserFromSession(request
				.getSession());
		ObjectMapper mapper = new ObjectMapper();
		String resp = null;
		try
		{
			resp = mapper.writeValueAsString(loggedInUser);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}
		logger.info("The user info is: " + resp);
		return resp;
	}
}
