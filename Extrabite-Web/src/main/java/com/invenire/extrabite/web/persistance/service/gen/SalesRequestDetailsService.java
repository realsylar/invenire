/**
 *
 */
package com.invenire.extrabite.web.persistance.service.gen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.core.persistance.dao.SalesRequestDetailsDAO;
import com.invenire.extrabite.core.persistance.entity.SalesRequestDetails;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class SalesRequestDetailsService
{

	private SalesRequestDetailsDAO	dao;

	/**
	 * @param dao
	 */
	@Autowired
	public SalesRequestDetailsService(SalesRequestDetailsDAO dao)
	{
		super();
		this.dao = dao;
	}

	/**
	 * @param requestDetails
	 */
	@Transactional
	public void saveSalesRequestDetails(SalesRequestDetails requestDetails)
	{
		dao.saveOrUpdateSalesRequestDetail(requestDetails);
	}

}
