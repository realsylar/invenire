/**
 *
 */
package com.invenire.extrabite.web.persistance.service.gen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.core.persistance.dao.RestaurantMenuInfoDAO;

/**
 * Service class for all functionalities on the Restaurant Menu Info entity
 *
 * @author Anurag Agrawal
 */
@Service
public class RestaurantMenuInfoService
{

	@SuppressWarnings("unused")
	private RestaurantMenuInfoDAO	restaurantMenuInfoDAO;

	/**
	 * Autowired constructor to automatically load the restaurantMenuInfoDAO
	 * object from the spring - hibernate configuration.
	 *
	 * @param restaurantMenuInfoDAO
	 */
	@Autowired
	public RestaurantMenuInfoService(RestaurantMenuInfoDAO restaurantMenuInfoDAO)
	{
		super();
		this.restaurantMenuInfoDAO = restaurantMenuInfoDAO;
	}
}
