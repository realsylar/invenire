/**
 *
 */
package com.invenire.extrabite.web.persistence.service.sec;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.web.persistence.dao.sec.ExtrabiteAuthoritiesDAO;
import com.invenire.extrabite.web.persistence.entity.sec.ExtrabiteAuthorities;

/**
 * Service class for all DB operations on the authorities table.
 *
 * @author Anurag Agrawal
 */
@Service
public class ExtrabiteAuthoritiesService
{

	private ExtrabiteAuthoritiesDAO	dao;

	/**
	 * Autowired constructor to automatically load the dao object from the
	 * spring - hibernate configuration.
	 *
	 * @param dao
	 */
	@Autowired
	public ExtrabiteAuthoritiesService(ExtrabiteAuthoritiesDAO dao)
	{
		super();
		this.dao = dao;
	}

	public void saveOrUpdateAuthoritry(ExtrabiteAuthorities auth)
	{
		dao.saveOrUpdateAuthoritry(auth);
	}

	@Transactional(readOnly = true)
	public ExtrabiteAuthorities getAuthorities(int id)
	{
		return dao.getAuthorities(id);
	}

	@Transactional(readOnly = true)
	public List<ExtrabiteAuthorities> getAllAuthorities()
	{
		return dao.getAllAuthorities();
	}

	@Transactional(readOnly = true)
	public List<ExtrabiteAuthorities> getAllAuthoritiesFromIds(
			List<Integer> authIds)
			{
		return dao.getAllAuthoritiesFromIds(authIds);
			}
}
