/**
 *
 */
package com.invenire.extrabite.web.persistance.service.gen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.core.persistance.dao.MenuDishCategoryInfoDAO;

/**
 * Service class for all functionalities on the Menu Dish Category Info entity
 *
 * @author Anurag Agrawal
 */
@Service
public class MenuDishCategoryInfoService
{

	@SuppressWarnings("unused")
	private MenuDishCategoryInfoDAO	menuDishCategoryInfoDAO;

	/**
	 * Autowired constructor to automatically load the menuDishCategoryInfoDAO
	 * object from the spring - hibernate configuration.
	 *
	 * @param menuDishCategoryInfoDAO
	 */
	@Autowired
	public MenuDishCategoryInfoService(
			MenuDishCategoryInfoDAO menuDishCategoryInfoDAO)
	{
		super();
		this.menuDishCategoryInfoDAO = menuDishCategoryInfoDAO;
	}
}
