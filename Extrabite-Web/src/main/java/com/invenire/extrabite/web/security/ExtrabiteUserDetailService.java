/**
 *
 */
package com.invenire.extrabite.web.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.web.persistance.service.gen.UserInfoService;
import com.invenire.extrabite.web.persistence.dao.sec.ExtrabiteUserRolesDAO;
import com.invenire.extrabite.web.persistence.entity.gen.UserInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service(value = "extrabiteUserDetailService")
public class ExtrabiteUserDetailService implements UserDetailsService
{

	private UserInfoService							userInfoService;
	private ExtrabiteUserRolesDAO					userRolesDAO;
	private ExtrabiteRolesAndAuthoritiesProvider	rolesAndAuthsProvider;

	/**
	 * @param userInfoService
	 * @param userRolesDAO
	 * @param roleAuthDAO
	 */
	@Autowired
	public ExtrabiteUserDetailService(UserInfoService userInfoService,
			ExtrabiteUserRolesDAO userRolesDAO,
			ExtrabiteRolesAndAuthoritiesProvider rolesAndAuthsProvider)
	{
		super();
		this.userInfoService = userInfoService;
		this.userRolesDAO = userRolesDAO;
		this.rolesAndAuthsProvider = rolesAndAuthsProvider;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException
	{
		return populateUserAuthorities(userInfoService
				.getUserByUserName(userName));
	}

	/**
	 * @param userByUserName
	 * @return
	 */
	private UserDetails populateUserAuthorities(UserInfo userByUserName)
	{
		List<Integer> roleIds = userRolesDAO.getAllRolesForUser(userByUserName
				.getUserId());
		userByUserName.setAuthorities(rolesAndAuthsProvider
				.getAllAuthoritiesForRoleIds(roleIds));
		return userByUserName;
	}
}
