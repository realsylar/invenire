/**
 *
 */
package com.invenire.extrabite.web.controllers.dashboard.dm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "com")
public class CampaignAndOfferManagementController
{

	@RequestMapping(method = RequestMethod.GET, value = "loadAllOffers")
	@ResponseBody
	public String loadAllOffers(HttpServletRequest request,
			HttpServletResponse response)
	{
		String resp = null;
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "loadAllCampaigns")
	@ResponseBody
	public String loadAllCampaigns(HttpServletRequest request,
			HttpServletResponse response)
	{
		String resp = null;
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "createCampaign")
	@ResponseBody
	public String createCampaign(HttpServletRequest request,
			HttpServletResponse response)
	{
		String resp = null;
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "createOffer")
	@ResponseBody
	public String createOffer(@RequestParam(value = "oType") int OType,
			HttpServletRequest request, HttpServletResponse response)
	{
		String resp = null;

		return resp;
	}
}
