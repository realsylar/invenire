/**
 *
 */
package com.invenire.extrabite.web.controllers.dashboard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.invenire.extrabite.core.cache.ClientToUserMappingCache;
import com.invenire.extrabite.core.dto.ClientLicenseSubscriptionDetailsDTO;
import com.invenire.extrabite.core.enums.PaymentStatus;
import com.invenire.extrabite.core.persistance.entity.ClientLicenseSubscriptionDetails;
import com.invenire.extrabite.core.persistance.entity.LicenseInfo;
import com.invenire.extrabite.web.persistance.service.gen.ClientLicenseSubscriptionDetailsService;
import com.invenire.extrabite.web.persistence.entity.gen.UserInfo;
import com.invenire.extrabite.web.sess.ExtrabiteWebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "subs")
public class SubscriptionDetailsController
{

	private static final Logger						logger	= Logger.getLogger(SubscriptionDetailsController.class);

	private ExtrabiteWebSessionManager				webSessionManager;
	private ClientToUserMappingCache				clientToUserMappingCache;
	private ClientLicenseSubscriptionDetailsService	clientLicenseSubscriptionDetailsService;

	/**
	 * @param webSessionManager
	 * @param clientToUserMappingCache
	 * @param clientLicenseSubscriptionDetailsService
	 */
	@Autowired
	public SubscriptionDetailsController(
			ExtrabiteWebSessionManager webSessionManager,
			ClientToUserMappingCache clientToUserMappingCache,
			ClientLicenseSubscriptionDetailsService clientLicenseSubscriptionDetailsService)
	{
		super();
		this.webSessionManager = webSessionManager;
		this.clientToUserMappingCache = clientToUserMappingCache;
		this.clientLicenseSubscriptionDetailsService = clientLicenseSubscriptionDetailsService;
	}

	@RequestMapping(method = RequestMethod.GET, value = "subsDetails")
	@ResponseBody
	public String getSubscriptionDetails(HttpServletRequest request,
			HttpServletResponse response)
	{
		UserInfo loggedInUser = webSessionManager.getUserFromSession(request
				.getSession());
		int clientId = clientToUserMappingCache.getProperty(loggedInUser
				.getUserId());
		ClientLicenseSubscriptionDetails subsDetails = clientLicenseSubscriptionDetailsService
				.getClientLicenseSubscriptionDetailsByClientId(clientId, true);
		LicenseInfo license = subsDetails.getLicense();
		ClientLicenseSubscriptionDetailsDTO dto = getSubscriptionDetailsDTO(
				subsDetails, license);
		logger.info("The DTO created is : " + dto);
		ObjectMapper mapper = new ObjectMapper();
		String resp = null;
		try
		{
			resp = mapper.writeValueAsString(dto);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}
		return resp;
	}

	/**
	 * @param subsDetails
	 * @param dto
	 * @return
	 */
	private ClientLicenseSubscriptionDetailsDTO getSubscriptionDetailsDTO(
			ClientLicenseSubscriptionDetails subsDetails, LicenseInfo license)
	{
		return new ClientLicenseSubscriptionDetailsDTO(
				subsDetails.getClientId(),
				subsDetails.getSubscriptionActivatedTimestamp(),
				license.getLicenseId(), license.getLicenseDisplayName(),
				license.getLicenseDescription(), subsDetails.isActive(),
				PaymentStatus.getEnumNameForValue(subsDetails
						.getPaymentStatus()));
	}

}
