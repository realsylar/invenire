/**
 *
 */
package com.invenire.extrabite.web.persistence.dao.sec;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO class for all DB operations on the user_roles table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class ExtrabiteUserRolesDAO
{

	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public ExtrabiteUserRolesDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

	public List<Integer> getAllRolesForUser(int userId)
	{
		Session sess = sessionFactory.getCurrentSession();
		List<Integer> auths = sess.createSQLQuery(
				"select role_id from user_roles where user_id = " + userId)
				.list();
		return auths;
	}
}
