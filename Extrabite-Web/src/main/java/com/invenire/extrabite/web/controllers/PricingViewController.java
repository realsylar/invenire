/**
 *
 */
package com.invenire.extrabite.web.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;
import com.invenire.commons.utils.aws.entity.SESMailingMessageParams;
import com.invenire.commons.utils.aws.enums.EmailBodyType;
import com.invenire.extrabite.core.enums.SalesRequestStatus;
import com.invenire.extrabite.core.persistance.entity.SalesRequestDetails;
import com.invenire.extrabite.core.utils.SpringContextPropertyLoader;
import com.invenire.extrabite.core.utils.email.ExtrabiteEmailSenderManager;
import com.invenire.extrabite.web.form.dataobject.ContactSalesFormData;
import com.invenire.extrabite.web.form.validators.ContactSalesFormDataValidator;
import com.invenire.extrabite.web.persistance.service.gen.SalesRequestDetailsService;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "pricing")
public class PricingViewController
{

	private ExtrabiteEmailSenderManager	emailSenderManager;
	private Validator					validator;
	private SalesRequestDetailsService	salesRequestDetailService;

	private static final Logger			logger										= Logger.getLogger(PricingViewController.class);
	/*
	 * private static final String FROM_ADDRESS =
	 * "support@inveniretechnologies.in"; private static final String TO_ADDRESS
	 * = "sales@inveniretechnologies.in";
	 */
	private static final String			FROM_ADDRESS								= "frequent.anurag@gmail.com";
	private static final String			TO_ADDRESS									= "anuragagrawal@outlook.com";
	private static final String			NEW_USER_CONTACT_SUBJECT					= "new.user.contact.subject";
	private static final String			NEW_USER_CONTACT_BODY						= "new.user.contact.body";
	private static final String			PRODUCT_PRICING_VIEWNAME					= "pricing";
	private static final String			CONTACT_SALES_SUCCESS						= "redirect:success";
	private static final String			CONTACT_SALES_SUCCESS_DIRECT_ACCESS_BLOCK	= "redirect:get";
	private static final String			CONTACT_SALES_SUCCESS_DIRECT_ACCESS_CHECK	= "cssdac";
	private static final String			DEFAULT_ERROR_MESSAGE						= "default.error.message";
	private static final String			FORM_SUBMIT_SUCCESS_MESSAGE					= "form.submit.success.message";

	/**
	 * @param emailSenderManager
	 */
	@Autowired
	public PricingViewController(
			ExtrabiteEmailSenderManager emailSenderManager,
			SalesRequestDetailsService salesRequestDetailService)
	{
		super();
		this.emailSenderManager = emailSenderManager;
		this.salesRequestDetailService = salesRequestDetailService;
		this.validator = new ContactSalesFormDataValidator();
	}

	@RequestMapping(value = "get", method = RequestMethod.GET)
	public ModelAndView loadThePricingPage(HttpServletRequest request,
			HttpServletResponse response)
	{
		logger.info("Inside the get method");
		return getProductPricingView();
	}

	@RequestMapping(value = "success", method = RequestMethod.GET)
	public ModelAndView showSuccessMessageWithLoadPricingPage(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model)
	{
		logger.info("Inside the success method");
		String param = (String) request.getSession().getAttribute(
				CONTACT_SALES_SUCCESS_DIRECT_ACCESS_CHECK);
		logger.info("Param value from the session is: " + param);
		model.clear();
		if (param != null && "true".equals(param))
			return getProductPricingViewSuccess();
		else
			return new ModelAndView(CONTACT_SALES_SUCCESS_DIRECT_ACCESS_BLOCK);
	}

	@RequestMapping(value = "submit", method = RequestMethod.POST)
	public String submitquery(
			@ModelAttribute("contactSalesFormData") @Validated ContactSalesFormData contactSalesFormData,
			BindingResult bindingResult, SessionStatus sessionStatus,
			ModelMap model, HttpServletRequest request)
	{
		logger.info("Inside the submit method");
		validator.validate(contactSalesFormData, bindingResult);
		if (bindingResult.hasErrors())
		{
			return PRODUCT_PRICING_VIEWNAME;
		}
		else
		{
			try
			{
				SalesRequestDetails requestDetails = getSalesRequestDetails(contactSalesFormData);
				salesRequestDetailService
				.saveSalesRequestDetails(requestDetails);
				SESMailingMessageParams messageParams = getMessageParams(requestDetails);
				emailSenderManager.sendMail(messageParams);
				request.getSession().setAttribute(
						CONTACT_SALES_SUCCESS_DIRECT_ACCESS_CHECK, "true");
			} catch (Exception e)
			{
				logger.error("Faced exception while sending request ", e);
				bindingResult.addError(new ObjectError("techDifficulty",
						SpringContextPropertyLoader
						.getPropertyForKey(DEFAULT_ERROR_MESSAGE)));
				return PRODUCT_PRICING_VIEWNAME;
			}
			sessionStatus.isComplete();
			model.clear();
			return CONTACT_SALES_SUCCESS;
		}
	}

	/**
	 * @return
	 */
	@ModelAttribute(value = "cityOptions")
	private List<String> getCityOptionsList()
	{
		return Lists.newArrayList("Indore", "Ahmedabad", "Hyderabad", "Pune");
	}

	/**
	 * @return
	 */
	private ModelAndView getProductPricingView()
	{
		ModelAndView view = new ModelAndView(PRODUCT_PRICING_VIEWNAME);
		view.addObject("contactSalesFormData", new ContactSalesFormData());
		return view;
	}

	/**
	 * @return
	 */
	private ModelAndView getProductPricingViewSuccess()
	{
		ModelAndView view = getProductPricingView();
		view.addObject("successMsg", SpringContextPropertyLoader
				.getPropertyForKey(FORM_SUBMIT_SUCCESS_MESSAGE));
		logger.info("sucess message is : "
				+ SpringContextPropertyLoader
				.getPropertyForKey(FORM_SUBMIT_SUCCESS_MESSAGE));
		logger.info("View is : " + view);
		return view;
	}

	/**
	 * @param contactSalesFormData
	 * @return
	 */
	private SESMailingMessageParams getMessageParams(
			SalesRequestDetails requestDetails)
	{
		List<String> toAddresses = new ArrayList<String>();
		toAddresses.add(TO_ADDRESS);
		return new SESMailingMessageParams(FROM_ADDRESS, toAddresses, null,
				null,
				SpringContextPropertyLoader
				.getPropertyForKey(NEW_USER_CONTACT_SUBJECT),
				SpringContextPropertyLoader.getPropertyForKey(
						NEW_USER_CONTACT_BODY).replace("%userData%",
								requestDetails.toString()), EmailBodyType.TEXTHTML);
	}

	/**
	 * @param contactSalesFormData
	 * @return
	 */
	private SalesRequestDetails getSalesRequestDetails(
			ContactSalesFormData contactSalesFormData)
	{
		SalesRequestDetails det = new SalesRequestDetails(
				contactSalesFormData.getUserName(),
				contactSalesFormData.getPhNo(),
				contactSalesFormData.getEmail(), contactSalesFormData.getCity());
		det.setComments(contactSalesFormData.getQuery());
		det.setRequestStatus(SalesRequestStatus.REQUEST_REGISTERED.getValue());
		return det;
	}
}
