/**
 *
 */
package com.invenire.extrabite.web.persistence.entity.gen;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * DB Entity class for managing user login details
 *
 * @author Anurag Agrawal
 */
@Entity
@Table(name = "user_login_info")
public class UserLoginInfo
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_login_id")
	private int			userLoginId;

	@Column(name = "creation_timestamp")
	private Timestamp	creationTimestamp;

	@Column(name = "modification_timestamp")
	private Timestamp	modificationTimestamp;

	@Column(name = "user_login_type")
	private int			userLoginType;

	@Column(name = "user_name")
	private String		userName;

	@Column(name = "password")
	private String		password;

	@Column(name = "salt")
	private String		salt;

	@Column(name = "access_token")
	private String		accessToken;

	/**
	 * @return the userLoginId
	 */
	public int getUserLoginId()
	{
		return userLoginId;
	}

	/**
	 * @param userLoginId
	 *            the userLoginId to set
	 */
	public void setUserLoginId(int userLoginId)
	{
		this.userLoginId = userLoginId;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp()
	{
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp
	 *            the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp)
	{
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the modificationTimestamp
	 */
	public Timestamp getModificationTimestamp()
	{
		return modificationTimestamp;
	}

	/**
	 * @param modificationTimestamp
	 *            the modificationTimestamp to set
	 */
	public void setModificationTimestamp(Timestamp modificationTimestamp)
	{
		this.modificationTimestamp = modificationTimestamp;
	}

	/**
	 * @return the userLoginType
	 */
	public int getUserLoginType()
	{
		return userLoginType;
	}

	/**
	 * @param userLoginType
	 *            the userLoginType to set
	 */
	public void setUserLoginType(int userLoginType)
	{
		this.userLoginType = userLoginType;
	}

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the accessToken
	 */
	public String getAccessToken()
	{
		return accessToken;
	}

	/**
	 * @param accessToken
	 *            the accessToken to set
	 */
	public void setAccessToken(String accessToken)
	{
		this.accessToken = accessToken;
	}

	/**
	 * @return the salt
	 */
	public String getSalt()
	{
		return salt;
	}

	/**
	 * @param salt
	 *            the salt to set
	 */
	public void setSalt(String salt)
	{
		this.salt = salt;
	}

}
