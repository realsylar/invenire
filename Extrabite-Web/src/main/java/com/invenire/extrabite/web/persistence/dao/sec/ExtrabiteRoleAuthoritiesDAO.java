/**
 *
 */
package com.invenire.extrabite.web.persistence.dao.sec;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO class for all DB operations on the role_authorities table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class ExtrabiteRoleAuthoritiesDAO
{

	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public ExtrabiteRoleAuthoritiesDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

	public List<Integer> getAllAuthoritiesForRole(int roleId)
	{
		Session sess = sessionFactory.getCurrentSession();
		List<Integer> auths = sess.createSQLQuery(
				"select auth_id from role_authorities where role_id = "
						+ roleId).list();
		return auths;
	}

}
