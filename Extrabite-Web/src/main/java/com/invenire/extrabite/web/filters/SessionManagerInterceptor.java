package com.invenire.extrabite.web.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.invenire.extrabite.web.sess.ExtrabiteWebSessionManager;

/**
 * Servlet Filter implementation class SessionManagerInterceptor
 */

public class SessionManagerInterceptor extends HandlerInterceptorAdapter
{

	private ExtrabiteWebSessionManager	sessionManager;

	/**
	 * @param sessionManager
	 */
	@Autowired
	public SessionManagerInterceptor(ExtrabiteWebSessionManager sessionManager)
	{
		super();
		this.sessionManager = sessionManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet
	 * .http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception
	{
		if (!sessionManager.isUserInSession(request.getSession()))
		{
			response.sendRedirect("/extrabiteweb/login/");
		}
		return true;
	}

}
