/**
 *
 */
package com.invenire.extrabite.web.sess;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.web.exception.UserIdNotValidException;
import com.invenire.extrabite.web.persistance.service.gen.UserInfoService;
import com.invenire.extrabite.web.persistence.entity.gen.UserInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ExtrabiteWebSessionManager
{

	private static final String	USER_SESSION_NAME	= "user";
	private UserInfoService		userInfoService;

	/**
	 * @param userInfoService
	 */
	@Autowired
	public ExtrabiteWebSessionManager(UserInfoService userInfoService)
	{
		super();
		this.userInfoService = userInfoService;
	}

	public void addUserToSession(HttpSession session, UserInfo user)
	{
		session.setAttribute(USER_SESSION_NAME, user);
		session.setMaxInactiveInterval(30 * 60);
	}

	public void addUserToSession(HttpSession session, String uuid)
			throws UserIdNotValidException
	{
		try
		{
			UserInfo user = userInfoService.getUser(Integer.valueOf(uuid));
			session.setAttribute(USER_SESSION_NAME, user);
			session.setMaxInactiveInterval(30 * 60);
		} catch (NumberFormatException e)
		{
			throw new UserIdNotValidException(e);
		}
	}

	public UserInfo getUserFromSession(HttpSession session)
	{
		return (UserInfo) session.getAttribute(USER_SESSION_NAME);
	}

	public boolean isUserInSession(HttpSession session)
	{
		return session.getAttribute(USER_SESSION_NAME) != null;
	}
}
