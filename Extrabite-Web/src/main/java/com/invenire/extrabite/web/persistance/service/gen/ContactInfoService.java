/**
 *
 */
package com.invenire.extrabite.web.persistance.service.gen;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.core.persistance.dao.ContactInfoDAO;
import com.invenire.extrabite.core.persistance.entity.ContactInfo;

/**
 * Service class for all functionalities on the Contact Info entity
 *
 * @author Anurag Agrawal
 */
@Service
public class ContactInfoService
{

	private ContactInfoDAO	contactInfoDAO;

	/**
	 * Autowired constructor to automatically load the contactInfoDAO object
	 * from the spring - hibernate configuration.
	 *
	 * @param contactInfoDAO
	 */
	@Autowired
	public ContactInfoService(ContactInfoDAO contactInfoDAO)
	{
		super();
		this.contactInfoDAO = contactInfoDAO;
	}

	/**
	 * Service method to get all the details of the Contacts which correspond to
	 * the same phone number
	 *
	 * @param phoneNumber
	 *            The phone number against which the contacts in the DB are
	 *            searched
	 * @return The list/set of the Contacts matched against the given phone
	 *         number. Null if none
	 */
	@Transactional(readOnly = true)
	public Collection<ContactInfo> getAllContactInfoForPhoneNumber(
			String phoneNumber)
			{
		return contactInfoDAO.getAllContactInfoForPhoneNumber(phoneNumber);
			}
}
