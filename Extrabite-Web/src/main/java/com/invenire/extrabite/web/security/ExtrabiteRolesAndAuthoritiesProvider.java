/**
 *
 */
package com.invenire.extrabite.web.security;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.invenire.extrabite.web.persistence.entity.sec.ExtrabiteAuthorities;
import com.invenire.extrabite.web.persistence.service.sec.ExtrabiteAuthoritiesService;
import com.invenire.extrabite.web.persistence.service.sec.ExtrabiteRoleAuthoritiesService;
import com.invenire.extrabite.web.persistence.service.sec.ExtrabiteRolesService;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ExtrabiteRolesAndAuthoritiesProvider
{

	private static final Logger							logger	= Logger.getLogger(ExtrabiteRolesAndAuthoritiesProvider.class);
	private Map<Integer, List<ExtrabiteAuthorities>>	roleIdVsAuthoritiesMap;
	private ExtrabiteRoleAuthoritiesService				roleAuthoritiesService;
	private ExtrabiteRolesService						rolesService;
	private ExtrabiteAuthoritiesService					authService;

	/**
	 * @param roleAuthoritiesDAO
	 */
	@Autowired
	public ExtrabiteRolesAndAuthoritiesProvider(
			ExtrabiteRoleAuthoritiesService roleAuthoritiesService,
			ExtrabiteRolesService rolesService,
			ExtrabiteAuthoritiesService authService)
	{
		super();
		this.roleAuthoritiesService = roleAuthoritiesService;
		this.rolesService = rolesService;
		this.authService = authService;
	}

	@PostConstruct
	private void loadRoleVsAuthorities()
	{
		roleIdVsAuthoritiesMap = Maps.newHashMap();
		List<Integer> roleIds = rolesService.getAllRolesIds();
		for (Integer roleId : roleIds)
		{
			List<Integer> authIdsForRole = roleAuthoritiesService
					.getAllAuthoritiesForRole(roleId);
			List<ExtrabiteAuthorities> auths = authService
					.getAllAuthoritiesFromIds(authIdsForRole);
			if (auths != null && !auths.isEmpty())
				roleIdVsAuthoritiesMap.put(roleId, auths);
		}
		logger.info("After loading the map of roles to auths is: "
				+ roleIdVsAuthoritiesMap);
	}

	public List<ExtrabiteAuthorities> getAllAuthoritiesForRoleIds(
			List<Integer> roleIds)
	{
		List<ExtrabiteAuthorities> auths = Lists.newArrayList();
		for (Integer roleId : roleIds)
		{
			auths.addAll(roleIdVsAuthoritiesMap.get(roleId));
		}
		return auths;
	}
}
