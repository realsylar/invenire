/**
 *
 */
package com.invenire.extrabite.web.controllers;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.invenire.extrabite.core.sec.enc.SHA256Encryptor;
import com.invenire.extrabite.core.utils.SpringContextPropertyLoader;
import com.invenire.extrabite.web.form.dataobject.ClientUserLoginDetail;
import com.invenire.extrabite.web.persistance.service.gen.UserInfoService;
import com.invenire.extrabite.web.persistence.entity.gen.UserInfo;
import com.invenire.extrabite.web.security.ExtrabiteCookieManager;
import com.invenire.extrabite.web.sess.ExtrabiteWebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "login")
public class LoginController
{

	private static final Logger			logger					= Logger.getLogger(LoginController.class);
	private static final String			LOGIN_PAGE_VIEW			= "login";
	private static final String			SUCCESS_USER_LOGIN		= "redirect:/dashboard/home";
	private static final String			DEFAULT_ERROR_MESSAGE	= "default.error.message";
	private static final String			USER_NOT_FOUND			= null;
	private static final String			PASSWORD_IS_WRONG		= null;
	private static final String			USER_DISABLED			= null;

	private UserInfoService				userInfoService;
	private SHA256Encryptor				passwordEncryptor;
	private ExtrabiteWebSessionManager	sessionManager;

	/**
	 * @param userInfoService
	 */
	@Autowired
	public LoginController(UserInfoService userInfoService,
			SHA256Encryptor passwordEncryptor,
			ExtrabiteWebSessionManager sessionManager)
	{
		super();
		this.userInfoService = userInfoService;
		this.passwordEncryptor = passwordEncryptor;
		this.sessionManager = sessionManager;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public ModelAndView getLoginForm(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = null;
		if (sessionManager.isUserInSession(request.getSession()))
		{
			model = new ModelAndView(SUCCESS_USER_LOGIN);
		}
		else
		{
			model = new ModelAndView(LOGIN_PAGE_VIEW);
			model.addObject("loginDetail", new ClientUserLoginDetail());
		}
		return model;
	}

	@RequestMapping(value = "auth", method = RequestMethod.POST)
	public String authenticateUser(
			@ModelAttribute("loginDetail") @Validated ClientUserLoginDetail loginDetail,
			BindingResult bindingResult, SessionStatus sessionStatus,
			HttpServletRequest request, ModelMap modelMap,
			HttpServletResponse response)
	{
		logger.info("The validated object received is : " + loginDetail);
		if (bindingResult.hasErrors())
		{
			return LOGIN_PAGE_VIEW;
		}
		else
		{
			try
			{
				UserInfo userInfo = userInfoService.getUserByEmail(loginDetail
						.getEmail());
				HttpSession session = request.getSession();
				String errors = validateUserLogin(loginDetail, userInfo);
				if (errors == null)
				{
					setOrUpdateSessionParameters(session, userInfo);
					logger.info("If the remember me is checked : "
							+ loginDetail.isRememberChecked()
							+ " going for adding user cookie. Otherwise only session.");
					if (loginDetail.isRememberChecked())
						response = ExtrabiteCookieManager.addUserCookie(
								response, userInfo);
				}
				else
				{
					logger.error("Faced exception while logging in " + errors);
					bindingResult.addError(new ObjectError("loginErrors",
							SpringContextPropertyLoader
							.getPropertyForKey(errors)));
					return LOGIN_PAGE_VIEW;
				}
			} catch (Exception e)
			{
				logger.error("Faced exception while logging in ", e);
				bindingResult.addError(new ObjectError("techDifficulty",
						SpringContextPropertyLoader
						.getPropertyForKey(DEFAULT_ERROR_MESSAGE)));
				return LOGIN_PAGE_VIEW;
			}
		}
		modelMap.clear();
		sessionStatus.isComplete();
		return SUCCESS_USER_LOGIN;
	}

	/**
	 * @param httpSession
	 *
	 */
	private void setOrUpdateSessionParameters(HttpSession httpSession,
			UserInfo user)
	{
		sessionManager.addUserToSession(httpSession, user);
	}

	/**
	 * @param loginDetail
	 * @param userInfo
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private String validateUserLogin(ClientUserLoginDetail loginDetail,
			UserInfo userInfo) throws InvalidKeyException,
			NoSuchAlgorithmException
	{
		if (userInfo != null)
		{
			if (userInfo.isActive()
					&& passwordEncryptor.encryptValue(
							loginDetail.getPassword(),
							userInfo.getLoginInfo().getSalt()).equals(
									userInfo.getLoginInfo().getPassword()))
			{
				return null;
			}
			else if (userInfo.isActive())
			{
				return PASSWORD_IS_WRONG;
			}
			else
			{
				return USER_DISABLED;
			}
		}
		else
		{
			return USER_NOT_FOUND;
		}
	}
}
