/**
 *
 */
package com.invenire.extrabite.web.form.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.invenire.commons.validation.valiators.EmailValidator;
import com.invenire.commons.validation.valiators.PasswordValidator;
import com.invenire.extrabite.web.form.dataobject.ClientUserLoginDetail;
import com.invenire.extrabite.web.form.dataobject.ContactSalesFormData;

/**
 * @author Anurag Agrawal
 *
 */
public class ClientUserLoginDetailValidator implements Validator
{

	private static final String	NULL_OBJECT				= "culd.null.object";
	private static final String	EMPTY_EMAIL				= "culd.empty.email";
	private static final String	EMPTY_PASS				= "culd.empty.pass";
	private static final String	PASS_NOT_VALID			= "culd.pass.not.valid";
	private static final String	EMAIL_NOT_VALID			= "culd.email.not.valid";
	private static final String	EMAIL_NOT_VERIFIABLE	= "culd.email.not.verifiable";

	@Override
	public boolean supports(Class<?> clazz)
	{
		return ClientUserLoginDetail.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		if (target == null || !(target instanceof ContactSalesFormData))
		{
			errors.reject(NULL_OBJECT);
		}
		else
		{
			ClientUserLoginDetail data = (ClientUserLoginDetail) target;
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email",
					EMPTY_EMAIL);
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
					EMPTY_PASS);
			if (!EmailValidator.isSemanticallyValid(data.getEmail()))
			{
				errors.rejectValue("email", EMAIL_NOT_VALID);
			}
			else if (!EmailValidator.isVerifiableValid(data.getEmail()))
			{
				errors.rejectValue("email", EMAIL_NOT_VERIFIABLE);
			}
			if (!PasswordValidator.isValid(data.getPassword()))
			{
				errors.rejectValue("password", PASS_NOT_VALID);
			}
		}
	}
}
