/**
 *
 */
package com.invenire.extrabite.web.persistence.dao.sec;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.invenire.extrabite.web.persistence.entity.sec.ExtrabiteRoles;

/**
 * DAO class for all DB operations on the roles table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class ExtrabiteRolesDAO
{

	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public ExtrabiteRolesDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

	public void saveOrUpdateRole(ExtrabiteRoles role)
	{
		Session sess = sessionFactory.getCurrentSession();
		sess.saveOrUpdate(role);
	}

	public ExtrabiteRoles getRole(int id)
	{
		Session sess = sessionFactory.getCurrentSession();
		ExtrabiteRoles role = (ExtrabiteRoles) sess.get(ExtrabiteRoles.class,
				id);
		return role;
	}

	public List<ExtrabiteRoles> getAllRoles()
	{
		Session sess = sessionFactory.getCurrentSession();
		List<ExtrabiteRoles> roles = sess.createCriteria(ExtrabiteRoles.class)
				.list();
		return roles;
	}

	public List<Integer> getAllRolesIds()
	{
		List<ExtrabiteRoles> roles = getAllRoles();
		List<Integer> roleIds = Lists.newArrayList();
		for (ExtrabiteRoles role : roles)
		{
			roleIds.add(role.getRoleId());
		}
		return roleIds;
	}
}
