/**
 *
 */
package com.invenire.extrabite.web.form.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.invenire.commons.validation.valiators.EmailValidator;
import com.invenire.commons.validation.valiators.PhoneNumberValidator;
import com.invenire.extrabite.web.form.dataobject.ContactSalesFormData;

/**
 * @author Anurag Agrawal
 *
 */
public class ContactSalesFormDataValidator implements Validator
{

	private static final String	NULL_OBJECT				= "csfd.null.object";
	private static final String	EMPTY_NAME				= "csfd.empty.name";
	private static final String	EMPTY_EMAIL				= "csfd.empty.email";
	private static final String	EMPTY_PHNO				= "csfd.empty.phno";
	private static final String	EMPTY_CITY				= "csfd.empty.city";
	private static final String	EMAIL_NOT_VALID			= "csfd.email.not.valid";
	private static final String	EMAIL_NOT_VERIFIABLE	= "csfd.email.not.verifiable";
	private static final String	PHNO_NOT_VALID			= "csfd.phno.not.valid";

	@Override
	public boolean supports(Class<?> clazz)
	{
		return ContactSalesFormData.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		if (target == null || !(target instanceof ContactSalesFormData))
		{
			errors.reject(NULL_OBJECT);
		}
		else
		{
			ContactSalesFormData data = (ContactSalesFormData) target;
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName",
					EMPTY_NAME);
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email",
					EMPTY_EMAIL);
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phNo",
					EMPTY_PHNO);
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city",
					EMPTY_CITY);
			if (!EmailValidator.isSemanticallyValid(data.getEmail()))
			{
				errors.rejectValue("email", EMAIL_NOT_VALID);
			}
			else if (!EmailValidator.isVerifiableValid(data.getEmail()))
			{
				errors.rejectValue("email", EMAIL_NOT_VERIFIABLE);
			}
			if (!PhoneNumberValidator.isValid(data.getPhNo()))
			{
				errors.rejectValue("phNo", PHNO_NOT_VALID);
			}
		}
	}
}
