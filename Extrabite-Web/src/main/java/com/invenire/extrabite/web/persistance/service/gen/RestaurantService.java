package com.invenire.extrabite.web.persistance.service.gen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.core.persistance.dao.RestaurantInfoDAO;
import com.invenire.extrabite.core.persistance.entity.RestaurantInfo;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class RestaurantService
{

	private RestaurantInfoDAO	restaurantDAO;

	/**
	 * @param restaurantDAO
	 */
	@Autowired
	public RestaurantService(RestaurantInfoDAO restaurantDAO)
	{
		this.restaurantDAO = restaurantDAO;
	}

	public RestaurantInfo getRestaurantById(int id)
	{
		return restaurantDAO.getRestaurantById(id);
	}
}
