/**
 *
 */
package com.invenire.extrabite.web.controllers.dashboard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "dashboard")
public class DashboardController
{

	private static final String	DASH_HOME_PAGE_VIEW	= "dashboard";

	@RequestMapping(method = RequestMethod.GET, value = "home")
	public ModelAndView showUserDashboardHome(HttpServletRequest request,
			HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView(DASH_HOME_PAGE_VIEW);
		return model;
	}

}
