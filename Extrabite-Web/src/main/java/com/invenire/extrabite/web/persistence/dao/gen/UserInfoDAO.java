package com.invenire.extrabite.web.persistence.dao.gen;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.invenire.extrabite.web.persistence.entity.gen.UserInfo;

/**
 * DAO class for all DB operations on the user_info table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class UserInfoDAO
{

	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public UserInfoDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

	/**
	 * DAO method to load the user info object from DB based on the identifier
	 * i.e. table primary key
	 *
	 * @param userId
	 *            The id of the user whose information has to be fetched
	 * @return The UserInfo object pertaining to the id. Null if not present
	 */
	public UserInfo getUser(int userId)
	{
		Session sess = sessionFactory.getCurrentSession();
		UserInfo user = (UserInfo) sess.get(UserInfo.class, userId);
		return user;
	}

	/**
	 * DAO method to load the user info object from DB based on the email of the
	 * user
	 *
	 * @param email
	 *            The email id of the user whose information has to be fetched
	 * @return The UserInfo object pertaining to the email id. Null if not
	 *         present
	 */
	public UserInfo getUserByEmail(String email)
	{
		Session sess = sessionFactory.getCurrentSession();
		Criteria crit = sess.createCriteria(UserInfo.class);
		crit.add(Restrictions.eq("email", email));
		UserInfo user = (UserInfo) crit.uniqueResult();
		return user;
	}

	/**
	 * DAO method to load the user info object from DB based on the user name of
	 * the user. Does an inner join and puts restriction on the second table
	 * field of user name. The inner join is done with the table user_login_info
	 *
	 * @param userName
	 *            The user name of the user whose information has to be fetched
	 * @return The UserInfo object pertaining to the user name. Null if not
	 *         present
	 */
	public UserInfo getUserByUserName(String userName)
	{
		Session sess = sessionFactory.getCurrentSession();
		Criteria crit = sess.createCriteria(UserInfo.class);
		crit.createAlias("loginInfo", "userlogin", JoinType.INNER_JOIN).add(
				Restrictions.eq("userlogin.userName", userName));
		UserInfo user = (UserInfo) crit.uniqueResult();
		return user;
	}

	/**
	 * DAO method to load the user info object from DB based on the mobile phone
	 * number of the user. Does an inner join and puts restriction on the second
	 * table field of user name. The inner join is done with the table
	 * contact_info
	 *
	 * @param phoneNumber
	 *            The phone number of the user whose information has to be
	 *            fetched
	 * @return The UserInfo object pertaining to the phone number. Null if not
	 *         present
	 */
	public UserInfo getUserByPhoneNumber(String phoneNumber)
	{
		Session sess = sessionFactory.getCurrentSession();
		Criteria crit = sess.createCriteria(UserInfo.class);
		crit.createAlias("contactInfo", "contactInfo", JoinType.INNER_JOIN)
				.add(Restrictions.eq("contactInfo.phoneNumber", phoneNumber));
		UserInfo user = (UserInfo) crit.uniqueResult();
		return user;
	}

	/**
	 * DAO method to persist the UserInfo object
	 *
	 * @param user
	 *            The UserInfo object to be persisted
	 * @return The integer user id value used to identify the persisted user
	 *         here on
	 */
	public int saveUserInfo(UserInfo user)
	{
		Session sess = sessionFactory.getCurrentSession();
		int userId = (Integer) sess.save(user);
		return userId;
	}

	/**
	 * DAO method to update an already saved user or persist a new user if no
	 * user already saved
	 *
	 * @param user
	 *            The user object to be updated or persisted
	 */
	public void saveOrUpdateUserInfo(UserInfo user)
	{
		Session sess = sessionFactory.getCurrentSession();
		sess.saveOrUpdate(user);
	}
}
