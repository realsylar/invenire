/**
 *
 */
package com.invenire.extrabite.web.persistance.service.gen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.core.persistance.dao.ClientLicenseSubscriptionDetailsDAO;
import com.invenire.extrabite.core.persistance.entity.ClientLicenseSubscriptionDetails;

/**
 * @author Anurag Agrawal
 *
 */
@Service
public class ClientLicenseSubscriptionDetailsService
{

	private ClientLicenseSubscriptionDetailsDAO	clientLicenseSubscriptionDetailsDAO;

	/**
	 * Autowired constructor to automatically load the dao object from the
	 * spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public ClientLicenseSubscriptionDetailsService(
			ClientLicenseSubscriptionDetailsDAO clientLicenseSubscriptionDetailsDAO)
	{
		super();
		this.clientLicenseSubscriptionDetailsDAO = clientLicenseSubscriptionDetailsDAO;
	}

	public int saveClientLicenseSubscriptionDetails(
			ClientLicenseSubscriptionDetails clientLicenseSubscriptionInfo)
	{
		int subsId = clientLicenseSubscriptionDetailsDAO
				.saveClientLicenseSubscriptionDetails(clientLicenseSubscriptionInfo);
		return subsId;
	}

	public void saveOrUpdateClientLicenseSubscriptionDetails(
			ClientLicenseSubscriptionDetails clientLicenseSubscriptionInfo)
	{
		clientLicenseSubscriptionDetailsDAO
				.saveOrUpdateClientLicenseSubscriptionDetails(clientLicenseSubscriptionInfo);
	}

	@Transactional(readOnly = true)
	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsById(
			int clientLicenseSubscriptionInfoId)
	{
		ClientLicenseSubscriptionDetails clientLicenseSubscriptionInfo = clientLicenseSubscriptionDetailsDAO
				.getClientLicenseSubscriptionDetailsById(clientLicenseSubscriptionInfoId);
		return clientLicenseSubscriptionInfo;
	}

	@Transactional(readOnly = true)
	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByClientId(
			int clientId)
	{
		ClientLicenseSubscriptionDetails subsInfo = clientLicenseSubscriptionDetailsDAO
				.getClientLicenseSubscriptionDetailsByClientId(clientId);
		return subsInfo;
	}

	@Transactional(readOnly = true)
	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByClientId(
			int clientId, boolean isActive)
	{
		ClientLicenseSubscriptionDetails subsInfo = clientLicenseSubscriptionDetailsDAO
				.getClientLicenseSubscriptionDetailsByClientId(clientId,
						isActive);
		return subsInfo;
	}

	@Transactional(readOnly = true)
	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByClientIdAndLicenseId(
			int clientId, int licenseId)
	{
		ClientLicenseSubscriptionDetails clientLicenseSubscriptionDetails = clientLicenseSubscriptionDetailsDAO
				.getClientLicenseSubscriptionDetailsByClientIdAndLicenseId(
						clientId, licenseId);
		return clientLicenseSubscriptionDetails;
	}

	@Transactional(readOnly = true)
	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByClientIdAndLicenseId(
			int clientId, int licenseId, boolean isActive)
	{
		ClientLicenseSubscriptionDetails clientLicenseSubscriptionDetails = clientLicenseSubscriptionDetailsDAO
				.getClientLicenseSubscriptionDetailsByClientIdAndLicenseId(
						clientId, licenseId, isActive);
		return clientLicenseSubscriptionDetails;
	}

	@Transactional(readOnly = true)
	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByLicenseId(
			int licenseId)
	{
		ClientLicenseSubscriptionDetails clientLicenseSubscriptionDetails = clientLicenseSubscriptionDetailsDAO
				.getClientLicenseSubscriptionDetailsByLicenseId(licenseId);
		return clientLicenseSubscriptionDetails;
	}

	@Transactional(readOnly = true)
	public ClientLicenseSubscriptionDetails getClientLicenseSubscriptionDetailsByLicenseId(
			int licenseId, boolean isActive)
	{
		ClientLicenseSubscriptionDetails clientLicenseSubscriptionDetails = clientLicenseSubscriptionDetailsDAO
				.getClientLicenseSubscriptionDetailsByLicenseId(licenseId,
						isActive);
		return clientLicenseSubscriptionDetails;
	}
}
