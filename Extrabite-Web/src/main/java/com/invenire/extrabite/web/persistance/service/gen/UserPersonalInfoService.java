/**
 *
 */
package com.invenire.extrabite.web.persistance.service.gen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.web.persistence.dao.gen.UserPersonalInfoDAO;

/**
 * Service class for all functionalities on the User Personal Info entity
 *
 * @author Anurag Agrawal
 */
@Service
public class UserPersonalInfoService
{

	@SuppressWarnings("unused")
	private UserPersonalInfoDAO	userPersonalInfoDAO;

	/**
	 * Autowired constructor to automatically load the userPersonalInfoDAO
	 * object from the spring - hibernate configuration.
	 *
	 * @param userPersonalInfoDAO
	 */
	@Autowired
	public UserPersonalInfoService(UserPersonalInfoDAO userPersonalInfoDAO)
	{
		super();
		this.userPersonalInfoDAO = userPersonalInfoDAO;
	}

}
