/**
 *
 */
package com.invenire.extrabite.web.persistance.service.gen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.web.persistence.dao.gen.UserLoginInfoDAO;

/**
 * Service class for all functionalities on the User Login Info entity
 *
 * @author Anurag Agrawal
 */
@Service
public class UserLoginInfoService
{

	@SuppressWarnings("unused")
	private UserLoginInfoDAO	userLoginInfoDAO;

	/**
	 * Autowired constructor to automatically load the userLoginInfoDAO object
	 * from the spring - hibernate configuration.
	 *
	 * @param userLoginInfoDAO
	 */
	@Autowired
	public UserLoginInfoService(UserLoginInfoDAO userLoginInfoDAO)
	{
		super();
		this.userLoginInfoDAO = userLoginInfoDAO;
	}

}
