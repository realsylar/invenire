/**
 *
 */
package com.invenire.extrabite.web.security;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.invenire.extrabite.web.persistence.entity.gen.UserInfo;

/**
 * @author Anurag Agrawal
 *
 */
public class ExtrabiteCookieManager
{

	private static final Logger	logger				= Logger.getLogger(ExtrabiteCookieManager.class);
	private static final String	USERID_COOKIE_NAME	= "ebuuid";

	public static int getUserFromCookie(HttpServletRequest request)
	{
		Cookie[] cookies = request.getCookies();
		logger.info("Cookies found are: " + cookies);
		if (cookies != null)
		{
			for (Cookie cookie : cookies)
			{
				logger.info("Cookie is: " + cookie.getName());
				if (cookie.getName().equals(USERID_COOKIE_NAME))
				{
					return Integer.parseInt(cookie.getValue());
				}
			}
		}
		return -1;
	}

	/**
	 * @param response
	 * @param userInfo
	 */
	public static HttpServletResponse addUserCookie(
			HttpServletResponse response, UserInfo userInfo)
	{
		Cookie cookie = new Cookie(USERID_COOKIE_NAME, String.valueOf(userInfo
				.getUserId()));
		cookie.setMaxAge(2592000);
		/* cookie.setDomain("extrabite.in"); */
		response.addCookie(cookie);
		return response;
	}
}
