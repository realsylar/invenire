/**
 *
 */
package com.invenire.extrabite.web.persistance.service.gen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invenire.extrabite.core.persistance.dao.RestaurantDisplayInfoDAO;

/**
 * Service class for all functionalities on the Restaurant Display Info entity
 *
 * @author Anurag Agrawal
 */
@Service
public class RestaurantDisplayInfoService
{

	@SuppressWarnings("unused")
	private RestaurantDisplayInfoDAO	restaurantDisplayInfoDAO;

	/**
	 * Autowired constructor to automatically load the restaurantDisplayInfoDAO
	 * object from the spring - hibernate configuration.
	 *
	 * @param restaurantDisplayInfoDAO
	 */
	@Autowired
	public RestaurantDisplayInfoService(
			RestaurantDisplayInfoDAO restaurantDisplayInfoDAO)
	{
		super();
		this.restaurantDisplayInfoDAO = restaurantDisplayInfoDAO;
	}

}
