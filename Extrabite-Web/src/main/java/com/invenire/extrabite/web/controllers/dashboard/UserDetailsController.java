/**
 *
 */
package com.invenire.extrabite.web.controllers.dashboard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.invenire.extrabite.core.dto.UserInfoDTO;
import com.invenire.extrabite.web.persistence.entity.gen.UserInfo;
import com.invenire.extrabite.web.sess.ExtrabiteWebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
@Controller
@RequestMapping(value = "user")
public class UserDetailsController
{

	private static final Logger			logger	= Logger.getLogger(UserDetailsController.class);

	private DozerBeanMapper				mapper;
	private ExtrabiteWebSessionManager	webSessionManager;

	/**
	 * @param mapper
	 * @param webSessionManager
	 */
	@Autowired
	public UserDetailsController(DozerBeanMapper mapper,
			ExtrabiteWebSessionManager webSessionManager)
	{
		super();
		this.mapper = mapper;
		this.webSessionManager = webSessionManager;
	}

	@RequestMapping(method = RequestMethod.GET, value = "userDetails")
	@ResponseBody
	public String getUserDetails(HttpServletRequest request,
			HttpServletResponse response)
	{
		UserInfo loggedInUser = webSessionManager.getUserFromSession(request
				.getSession());
		UserInfoDTO userDTO = mapper.map(loggedInUser, UserInfoDTO.class);
		ObjectMapper mapper = new ObjectMapper();
		String resp = null;
		try
		{
			resp = mapper.writeValueAsString(userDTO);
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}
		logger.info("The user info is: " + resp);
		return resp;
	}
}
