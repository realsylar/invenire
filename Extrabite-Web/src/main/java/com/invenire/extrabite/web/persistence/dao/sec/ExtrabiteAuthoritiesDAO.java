/**
 *
 */
package com.invenire.extrabite.web.persistence.dao.sec;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.invenire.extrabite.web.persistence.entity.sec.ExtrabiteAuthorities;

/**
 * DAO class for all DB operations on the authorities table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class ExtrabiteAuthoritiesDAO
{

	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public ExtrabiteAuthoritiesDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

	public void saveOrUpdateAuthoritry(ExtrabiteAuthorities auth)
	{
		Session sess = sessionFactory.getCurrentSession();
		sess.saveOrUpdate(auth);
	}

	public ExtrabiteAuthorities getAuthorities(int id)
	{
		Session sess = sessionFactory.getCurrentSession();
		ExtrabiteAuthorities auth = (ExtrabiteAuthorities) sess.get(
				ExtrabiteAuthorities.class, id);
		return auth;
	}

	public List<ExtrabiteAuthorities> getAllAuthorities()
	{
		Session sess = sessionFactory.getCurrentSession();
		List<ExtrabiteAuthorities> auth = sess.createCriteria(
				ExtrabiteAuthorities.class).list();
		return auth;
	}

	public List<ExtrabiteAuthorities> getAllAuthoritiesFromIds(
			List<Integer> authIds)
			{
		Session sess = sessionFactory.getCurrentSession();
		Criteria crit = sess.createCriteria(ExtrabiteAuthorities.class);
		Disjunction disj = Restrictions.disjunction();
		for (Integer authId : authIds)
		{
			disj.add(Restrictions.eq("id", authId));
		}
		crit.add(disj);
		List<ExtrabiteAuthorities> auth = crit.list();
		return auth;
			}
}
