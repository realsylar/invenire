/**
 *
 */
package com.invenire.extrabite.web.persistence.service.sec;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invenire.extrabite.web.persistence.dao.sec.ExtrabiteRolesDAO;
import com.invenire.extrabite.web.persistence.entity.sec.ExtrabiteRoles;

/**
 * Service class for all DB operations on the roles table.
 *
 * @author Anurag Agrawal
 */
@Service
public class ExtrabiteRolesService
{

	private ExtrabiteRolesDAO	dao;
	//private static final Logger	logger	= Logger.getLogger(ExtrabiteRolesService.class);

	/**
	 * Autowired constructor to automatically load the dao object from the
	 * spring - hibernate configuration.
	 *
	 * @param dao
	 */
	@Autowired
	public ExtrabiteRolesService(ExtrabiteRolesDAO dao)
	{
		super();
		this.dao = dao;
	}

	public void saveOrUpdateRole(ExtrabiteRoles role)
	{
		dao.saveOrUpdateRole(role);
	}

	@Transactional(readOnly = true)
	public ExtrabiteRoles getRole(int id)
	{
		return dao.getRole(id);
	}

	@Transactional(readOnly = true)
	public List<ExtrabiteRoles> getAllRoles()
	{
		return dao.getAllRoles();
	}

	@Transactional(readOnly = true)
	public List<Integer> getAllRolesIds()
	{
		return dao.getAllRolesIds();
	}
}
