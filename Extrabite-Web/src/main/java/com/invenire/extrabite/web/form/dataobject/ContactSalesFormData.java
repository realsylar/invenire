/**
 *
 */
package com.invenire.extrabite.web.form.dataobject;

/**
 * @author Anurag Agrawal
 *
 */
public class ContactSalesFormData
{

	private String	userName;
	private String	email;
	private String	phNo;
	private String	city;
	private String	query;

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the phNo
	 */
	public String getPhNo()
	{
		return phNo;
	}

	/**
	 * @param phNo
	 *            the phNo to set
	 */
	public void setPhNo(String phNo)
	{
		this.phNo = phNo;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * @return the query
	 */
	public String getQuery()
	{
		return query;
	}

	/**
	 * @param query
	 *            the query to set
	 */
	public void setQuery(String query)
	{
		this.query = query;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "ContactSalesFormData [userName=" + userName + ", email="
				+ email + ", phNo=" + phNo + ", city=" + city + ", query="
				+ query + "]";
	}

}
