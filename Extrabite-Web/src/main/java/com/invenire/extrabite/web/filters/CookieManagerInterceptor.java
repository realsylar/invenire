/**
 *
 */
package com.invenire.extrabite.web.filters;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.invenire.extrabite.web.sess.ExtrabiteWebSessionManager;

/**
 * @author Anurag Agrawal
 *
 */
public class CookieManagerInterceptor extends HandlerInterceptorAdapter
{

	private ExtrabiteWebSessionManager	sessionManager;
	private static final String			USERID_COOKIE_NAME	= "ebuuid";

	/**
	 * @param sessionManager
	 */
	@Autowired
	public CookieManagerInterceptor(ExtrabiteWebSessionManager sessionManager)
	{
		super();
		this.sessionManager = sessionManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle
	 * (javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception
	{
		if (!sessionManager.isUserInSession(request.getSession())
				&& request.getCookies() != null)
		{
			for (Cookie cook : request.getCookies())
			{
				if (cook != null && USERID_COOKIE_NAME.equals(cook.getName())
						&& cook.getValue() != null)
				{
					sessionManager.addUserToSession(request.getSession(),
							cook.getValue());
				}
			}
		}
		return true;
	}
}
