/**
 *
 */
package com.invenire.extrabite.web.persistence.dao.gen;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO class for all DB operations on the user_personal_info table.
 *
 * @author Anurag Agrawal
 */
@Repository
public class UserPersonalInfoDAO
{

	@SuppressWarnings("unused")
	private SessionFactory	sessionFactory;

	/**
	 * Autowired constructor to automatically load the session factory object
	 * from the spring - hibernate configuration.
	 *
	 * @param sessionFactory
	 */
	@Autowired
	public UserPersonalInfoDAO(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}

}
