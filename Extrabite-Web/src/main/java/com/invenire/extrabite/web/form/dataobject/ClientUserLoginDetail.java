/**
 *
 */
package com.invenire.extrabite.web.form.dataobject;

/**
 * @author Anurag Agrawal
 *
 */
public class ClientUserLoginDetail
{

	private String	email;
	private String	password;
	private boolean	rememberChecked;

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the rememberChecked
	 */
	public boolean isRememberChecked()
	{
		return rememberChecked;
	}

	/**
	 * @param rememberChecked
	 *            the rememberChecked to set
	 */
	public void setRememberChecked(boolean rememberChecked)
	{
		this.rememberChecked = rememberChecked;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "ClientUserLoginDetail [email=" + email + ", password="
				+ password + ", rememberChecked=" + rememberChecked + "]";
	}

}
